package hasher

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"hash/fnv"
	"strconv"
)

func GetHashID(id int) (string, error) {
	idStr := strconv.Itoa(id)
	h := fnv.New32a()
	_, err := h.Write([]byte(idStr))
	if err != nil {
		return "", err
	}
	hashValue := h.Sum32()
	hashID := fmt.Sprintf("%x", hashValue)
	return hashID, nil
}

func GetHashPassword(password string) (string, error) {
	hash := sha256.New()
	_, err := hash.Write([]byte(password))
	if err != nil {
		return "", err
	}
	hashBytes := hash.Sum(nil)
	hashPass := hex.EncodeToString(hashBytes)
	return hashPass, nil
}
