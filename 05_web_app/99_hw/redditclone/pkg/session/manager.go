package session

import (
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/hasher"
	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/user"
)

type SessionManager struct {
	mu            *sync.RWMutex
	sessions      map[string]*Session
	secret        []byte
	lastSessionID int
}

func NewSessionManager() *SessionManager {
	mySecret := os.Getenv("SECRET")
	return &SessionManager{
		mu:       &sync.RWMutex{},
		sessions: make(map[string]*Session),
		secret:   []byte(mySecret),
	}
}

func (sm *SessionManager) newToken(user *user.User) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user": user,
		"iat":  time.Now().Unix(),
		"exp":  time.Now().Add(time.Hour * 24 * 7).Unix(),
	})
	tokenString, err := token.SignedString(sm.secret)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func (sm *SessionManager) CreateNewSession(u *user.User) (string, error) {
	sm.mu.Lock()
	defer sm.mu.Unlock()
	sm.lastSessionID++
	hashedID, err := hasher.GetHashID(sm.lastSessionID)
	if err != nil {
		return "", err
	}
	newSession := newSession(u, hashedID)
	token, err := sm.newToken(u)
	if err != nil {
		return "", err
	}
	sm.sessions[token] = newSession
	return token, nil
}

func (sm *SessionManager) GetSession(inToken string) (*Session, error) {
	hashSecretGetter := func(token *jwt.Token) (interface{}, error) {
		method, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok || method.Alg() != "HS256" {
			return nil, fmt.Errorf("bad sign method")
		}
		return sm.secret, nil
	}
	token, err := jwt.Parse(inToken, hashSecretGetter)
	if err != nil || !token.Valid {
		return nil, ErrNoAuth
	}
	sm.mu.RLock()
	currentSession, ok := sm.sessions[inToken]
	sm.mu.RUnlock()
	if !ok || currentSession == nil {
		return nil, ErrNoAuth
	}
	return currentSession, nil
}
