package session

import (
	"errors"

	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/user"
)

type Session struct {
	ID   string
	User *user.User
}

var (
	ErrNoAuth = errors.New("no session found")
)

func newSession(user *user.User, id string) *Session {
	return &Session{
		ID:   id,
		User: user,
	}
}
