package post

import (
	"errors"
	"regexp"

	"github.com/asaskevich/govalidator"

	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/comment"
	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/user"
	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/vote"
)

var (
	ErrNoPost   = errors.New("no post found")
	ErrNoAccess = errors.New("forbidden action")
)

type Post struct {
	Score            int                `json:"score"`
	Views            int                `json:"views"`
	Type             string             `json:"type" valid:"required,in(text|link)"`
	Title            string             `json:"title" valid:"required,length(1|100)"`
	URL              string             `json:"url,omitempty" valid:"url"`
	Author           *user.User         `json:"author"`
	Category         string             `json:"category" valid:"required,length(1|300)"`
	Text             string             `json:"text,omitempty"`
	Votes            []*vote.Vote       `json:"votes"`
	Comments         []*comment.Comment `json:"comments"`
	Created          string             `json:"created"`
	UpvotePercentage int                `json:"upvotePercentage"`
	ID               string             `json:"id"`
}

type PostRepo interface {
	GetAll() ([]Post, error)
	AddPost(post *Post, author *user.User) (Post, error)
	GetPostByCategory(category string) ([]Post, error)
	GetPostByID(ID string) (Post, error)
	AddComment(commentBody string, author *user.User, postID string) (Post, error)
	DeleteComment(userID, postID string, commentID string) (Post, error)
	UpVote(postID string, userID string) (Post, error)
	DownVote(postID string, userID string) (Post, error)
	UnVote(postID string, userID string) (Post, error)
	DeletePost(userID, postID string) (bool, error)
	GetPostsByUserID(userName string) ([]Post, error)
}

func init() {
	govalidator.CustomTypeTagMap.Set("url", govalidator.CustomTypeValidator(func(i interface{}, o interface{}) bool {
		subject, ok := i.(string)
		if !ok {
			return false
		}
		urlPattern := `^(http|https):\/\/[a-zA-Z0-9.-]+(\.[a-zA-Z]{2,}){1,}([\w\W]*)$`
		re := regexp.MustCompile(urlPattern)
		return re.MatchString(subject)
	}))

}

func (p *Post) Validate() []string {
	_, err := govalidator.ValidateStruct(p)
	validationErrors := make([]string, 0)
	if err == nil {
		return validationErrors
	}
	if allErrs, ok := err.(govalidator.Errors); ok {
		for _, fld := range allErrs {
			validationErrors = append(validationErrors, fld.Error())
		}
	}
	if p.Type == "text" {
		if p.Text == "" {
			validationErrors = append(validationErrors, "text field required")

		}
	} else if p.URL == "" {
		validationErrors = append(validationErrors, "url field required")
	}
	return validationErrors

}
