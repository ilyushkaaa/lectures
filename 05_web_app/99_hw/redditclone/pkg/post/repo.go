package post

import (
	"sync"
	"time"

	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/comment"
	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/hasher"
	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/user"
	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/vote"
)

type PostMemoryRepository struct {
	posts  []*Post
	mu     *sync.RWMutex
	lastID int
}

func NewPostMemoryRepository() *PostMemoryRepository {
	return &PostMemoryRepository{
		posts: make([]*Post, 0),
		mu:    &sync.RWMutex{},
	}
}

func getTimeOfCreation() string {
	timeOfCreation := time.Now()
	return timeOfCreation.Format("2006-01-02T15:04:05.999Z")

}

func (p *PostMemoryRepository) GetAll() ([]Post, error) {
	p.mu.RLock()
	defer p.mu.RUnlock()
	allPosts := make([]Post, 0, len(p.posts))
	for _, curPost := range p.posts {
		postToReturn := copyPost(*curPost)
		allPosts = append(allPosts, postToReturn)
	}
	return allPosts, nil
}

func (p *PostMemoryRepository) AddPost(post *Post, author *user.User) (Post, error) {
	p.mu.Lock()
	defer p.mu.Unlock()
	p.lastID++
	id := p.lastID
	var err error
	post.ID, err = hasher.GetHashID(id)
	if err != nil {
		return Post{}, err
	}
	post.Author = author
	post.Votes = make([]*vote.Vote, 0, 1)
	post.Votes = append(post.Votes, &vote.Vote{
		Value:  1,
		UserID: author.ID,
	})
	if post.Type == "text" {
		post.URL = ""
	} else {
		post.Text = ""
	}
	post.Views = 0
	post.Comments = make([]*comment.Comment, 0)
	post.Created = getTimeOfCreation()
	post.UpvotePercentage = 100
	post.Score = 1
	p.posts = append(p.posts, post)
	postToReturn := copyPost(*post)
	return postToReturn, nil
}

func (p *PostMemoryRepository) GetPostByCategory(category string) ([]Post, error) {
	postOfCurrentCategory := make([]Post, 0)
	p.mu.RLock()
	defer p.mu.RUnlock()
	for _, post := range p.posts {
		if post.Category == category {
			postToReturn := copyPost(*post)
			postOfCurrentCategory = append(postOfCurrentCategory, postToReturn)
		}
	}
	return postOfCurrentCategory, nil

}

func (p *PostMemoryRepository) GetPostByID(id string) (Post, error) {
	p.mu.RLock()
	defer p.mu.RUnlock()
	for _, post := range p.posts {
		if post.ID == id {
			post.Views++
			postToReturn := copyPost(*post)
			return postToReturn, nil
		}
	}
	return Post{}, ErrNoPost
}

func (p *PostMemoryRepository) AddComment(commentBody string, author *user.User, postID string) (Post, error) {
	p.mu.Lock()
	defer p.mu.Unlock()
	for _, post := range p.posts {
		if post.ID == postID {
			p.lastID++
			newComment := &comment.Comment{}
			newComment.Body = commentBody
			newComment.Author = author
			var err error
			newComment.ID, err = hasher.GetHashID(p.lastID)
			if err != nil {
				return Post{}, err
			}
			newComment.Created = getTimeOfCreation()
			post.Comments = append(post.Comments, newComment)
			postToReturn := copyPost(*post)
			return postToReturn, nil
		}
	}
	return Post{}, ErrNoPost
}

func (p *PostMemoryRepository) DeleteComment(userID, postID, commentID string) (Post, error) {
	p.mu.Lock()
	defer p.mu.Unlock()
	for _, post := range p.posts {
		if post.ID == postID {
			for i, currentComment := range post.Comments {
				if currentComment.ID == commentID {
					if userID != currentComment.Author.ID {
						return *post, ErrNoAccess
					}
					post.Comments = append(post.Comments[:i], post.Comments[i+1:]...)
					postToReturn := copyPost(*post)
					return postToReturn, nil
				}
			}

		}
	}
	return Post{}, ErrNoPost
}

func (p *PostMemoryRepository) UpVote(postID string, userID string) (Post, error) {
	p.mu.Lock()
	defer p.mu.Unlock()
	for _, post := range p.posts {
		if post.ID == postID {
			for _, currentVote := range post.Votes {
				if currentVote.UserID == userID {
					if currentVote.Value == -1 {
						currentVote.Value = 1
						post.Score += 2
						post.UpvotePercentage = countUpVotePercentage(post)
					}
					postToReturn := copyPost(*post)
					return postToReturn, nil
				}
			}
			post.Votes = append(post.Votes, vote.NewVote(1, userID))
			post.Score++
			post.UpvotePercentage = countUpVotePercentage(post)
			postToReturn := copyPost(*post)
			return postToReturn, nil
		}
	}
	return Post{}, ErrNoPost
}

func (p *PostMemoryRepository) DownVote(postID string, userID string) (Post, error) {
	p.mu.Lock()
	defer p.mu.Unlock()
	for _, post := range p.posts {
		if post.ID == postID {
			for _, currentVote := range post.Votes {
				if currentVote.UserID == userID {
					if currentVote.Value == 1 {
						post.Score -= 2
						currentVote.Value = -1
						post.UpvotePercentage = countUpVotePercentage(post)
					}
					postToReturn := copyPost(*post)
					return postToReturn, nil
				}
			}
			post.Votes = append(post.Votes, vote.NewVote(-1, userID))
			post.Score--
			post.UpvotePercentage = countUpVotePercentage(post)
			postToReturn := copyPost(*post)
			return postToReturn, nil
		}
	}
	return Post{}, ErrNoPost
}

func (p *PostMemoryRepository) UnVote(postID string, userID string) (Post, error) {
	p.mu.Lock()
	defer p.mu.Unlock()
	for _, post := range p.posts {
		if post.ID == postID {
			for i, currentVote := range post.Votes {
				if currentVote.UserID == userID {
					post.Votes = append(post.Votes[:i], post.Votes[i+1:]...)
					if len(post.Votes) == 0 {
						post.Score = 0
						post.UpvotePercentage = 0
					} else {
						if currentVote.Value == 1 {
							post.Score--
							post.UpvotePercentage = countUpVotePercentage(post)
						} else {
							post.Score++
							post.UpvotePercentage = countUpVotePercentage(post)
						}

					}
					postToReturn := copyPost(*post)
					return postToReturn, nil
				}
			}
			postToReturn := copyPost(*post)
			return postToReturn, nil
		}
	}
	return Post{}, ErrNoPost
}

func (p *PostMemoryRepository) DeletePost(userID, postID string) (bool, error) {
	p.mu.Lock()
	defer p.mu.Unlock()
	for i, post := range p.posts {
		if post.ID == postID {
			if post.Author.ID != userID {
				return false, ErrNoAccess
			}
			p.posts = append(p.posts[:i], p.posts[i+1:]...)
			return true, nil
		}

	}
	return false, ErrNoPost
}

func (p *PostMemoryRepository) GetPostsByUserID(userName string) ([]Post, error) {
	p.mu.RLock()
	defer p.mu.RUnlock()
	userPosts := make([]Post, 0)
	for _, userPost := range p.posts {
		if userPost.Author.Username == userName {
			postToReturn := copyPost(*userPost)
			userPosts = append(userPosts, postToReturn)
		}
	}
	return userPosts, nil
}

func countUpVotePercentage(postToCount *Post) int {
	var numOfUpVotes int
	for _, currentVote := range postToCount.Votes {
		if currentVote.Value == 1 {
			numOfUpVotes++
		}
	}
	return 100 * numOfUpVotes / len(postToCount.Votes)
}

func copyVotes(post Post) []*vote.Vote {
	copiedVotes := make([]*vote.Vote, 0, len(post.Votes))
	for _, curVote := range post.Votes {
		copiedVotes = append(copiedVotes, &vote.Vote{
			Value:  curVote.Value,
			UserID: curVote.UserID,
		})
	}
	return copiedVotes
}

func copyComments(post Post) []*comment.Comment {
	copiedComments := make([]*comment.Comment, 0, len(post.Comments))
	for _, curComment := range post.Comments {
		copiedComments = append(copiedComments, &comment.Comment{
			Created: curComment.Created,
			Author:  curComment.Author,
			Body:    curComment.Body,
			ID:      curComment.ID,
		})
	}
	return copiedComments
}
func copyAuthor(post Post) *user.User {
	copiedAuthor := &user.User{
		ID:       post.Author.ID,
		Username: post.Author.Username,
	}
	return copiedAuthor
}
func copyPost(post Post) Post {
	post.Votes = copyVotes(post)
	post.Comments = copyComments(post)
	post.Author = copyAuthor(post)
	return post
}
