package user

import (
	"errors"
	"sync"

	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/hasher"
)

var (
	ErrNoUser       = errors.New("no user with such username")
	ErrBadPass      = errors.New("bad password")
	ErrAlreadyExist = errors.New("already exists")
)

type UserMemoryRepository struct {
	lastUserID int
	users      map[string]*User
	mu         *sync.RWMutex
}

func NewUserMemoryRepository() *UserMemoryRepository {
	return &UserMemoryRepository{
		mu:    &sync.RWMutex{},
		users: make(map[string]*User),
	}
}

func (u *UserMemoryRepository) Login(username, password string) (*User, error) {
	currentUser, exist := u.users[username]
	if !exist {
		return nil, ErrNoUser
	}
	hashedPassword, err := hasher.GetHashPassword(password)
	if err != nil {
		return nil, err
	}
	if hashedPassword != currentUser.password {
		return nil, ErrBadPass
	}
	return currentUser, nil
}

func (u *UserMemoryRepository) Register(username, password string) (*User, error) {
	u.mu.Lock()
	defer u.mu.Unlock()
	if _, exists := u.users[username]; exists {
		return nil, ErrAlreadyExist
	}
	u.lastUserID++
	hashedID, err := hasher.GetHashID(u.lastUserID)
	if err != nil {
		return nil, err
	}
	hashedPassword, err := hasher.GetHashPassword(password)
	if err != nil {
		return nil, err
	}
	newUser := newUser(hashedID, username, hashedPassword)
	u.users[username] = newUser
	return newUser, nil

}
