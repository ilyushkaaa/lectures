package main

import (
	"github.com/gorilla/mux"
	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/middleware"
	"go.uber.org/zap"
	"html/template"
	"log"
	"net/http"

	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/handlers"
	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/post"
	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/session"
	"gitlab.com/vk-golang/lectures/05_web_app/99_hw/redditclone/pkg/user"
)

func main() {
	myTemplate := template.Must(template.ParseGlob("./05_web_app/99_hw/redditclone/static/html/*"))
	sessionManager := session.NewSessionManager()
	zapLogger, err := zap.NewProduction()
	if err != nil {
		log.Printf("error in logger initialization: %s", err)
		return
	}
	defer func(zapLogger *zap.Logger) {
		err = zapLogger.Sync()
		if err != nil {
			log.Printf("error in logger close: %s", err)
			return
		}
	}(zapLogger)
	logger := zapLogger.Sugar()

	userRepo := user.NewUserMemoryRepository()
	postRepo := post.NewPostMemoryRepository()

	userHandler := handlers.UserHandler{
		Template:       myTemplate,
		UserRepo:       userRepo,
		SessionManager: sessionManager,
		Logger:         logger,
	}

	postHandler := handlers.PostHandler{
		Template: myTemplate,
		PostRepo: postRepo,
		Logger:   logger,
	}

	router := mux.NewRouter()

	staticRouter := router.PathPrefix("/static/").Subrouter()
	staticDir := "./05_web_app/99_hw/redditclone/static"
	staticRouter.PathPrefix("/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(staticDir))))

	router.HandleFunc("/api/posts/", postHandler.List).Methods(http.MethodGet)
	router.HandleFunc("/api/posts/{CATEGORY_NAME}", postHandler.ListByCategory).Methods(http.MethodGet)
	router.HandleFunc("/api/post/{POST_ID}", postHandler.GetPostInfo).Methods(http.MethodGet)
	router.HandleFunc("/api/user/{USER_LOGIN}", postHandler.ListByUserLogin).Methods(http.MethodGet)

	router.HandleFunc("/api/register", userHandler.Register).Methods(http.MethodPost)
	router.HandleFunc("/api/login", userHandler.Login).Methods(http.MethodPost)

	// нужна авторизация

	rAuth := mux.NewRouter()
	router.Handle("/api/post/{POST_ID}/{COMMENT_ID}", middleware.Auth(logger, sessionManager, rAuth)).Methods(http.MethodDelete)
	router.Handle("/api/post/{POST_ID}/upvote", middleware.Auth(logger, sessionManager, rAuth)).Methods(http.MethodGet)
	router.Handle("/api/post/{POST_ID}/downvote", middleware.Auth(logger, sessionManager, rAuth)).Methods(http.MethodGet)
	router.Handle("/api/post/{POST_ID}/unvote", middleware.Auth(logger, sessionManager, rAuth)).Methods(http.MethodGet)
	router.Handle("/api/post/{POST_ID}", middleware.Auth(logger, sessionManager, rAuth)).Methods(http.MethodDelete)
	router.Handle("/api/posts", middleware.Auth(logger, sessionManager, rAuth)).Methods(http.MethodPost)
	router.Handle("/api/post/{POST_ID}", middleware.Auth(logger, sessionManager, rAuth)).Methods(http.MethodPost)

	rAuth.HandleFunc("/api/post/{POST_ID}/{COMMENT_ID}", postHandler.DeleteComment).Methods(http.MethodDelete)
	rAuth.HandleFunc("/api/post/{POST_ID}/upvote", postHandler.MakeVote).Methods(http.MethodGet)
	rAuth.HandleFunc("/api/post/{POST_ID}/downvote", postHandler.MakeVote).Methods(http.MethodGet)
	rAuth.HandleFunc("/api/post/{POST_ID}/unvote", postHandler.MakeVote).Methods(http.MethodGet)
	rAuth.HandleFunc("/api/post/{POST_ID}", postHandler.DeletePost).Methods(http.MethodDelete)
	rAuth.HandleFunc("/api/posts", postHandler.NewPost).Methods(http.MethodPost)
	rAuth.HandleFunc("/api/post/{POST_ID}", postHandler.NewComment).Methods(http.MethodPost)

	accessLogRouter := middleware.AccessLog(logger, router)
	errorLogRouter := middleware.ErrorLog(logger, accessLogRouter)
	mux := middleware.Panic(logger, errorLogRouter)

	router.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		err = myTemplate.ExecuteTemplate(w, "index.html", struct{}{})
		if err != nil {
			http.Error(w, `Template errror`, http.StatusInternalServerError)
		}
	})

	addr := ":8030"

	logger.Infow("starting server",
		"type", "START",
		"addr", addr,
	)
	err = http.ListenAndServe(addr, mux)
	if err != nil {
		logger.Infof("errror in server start")
	}
}
