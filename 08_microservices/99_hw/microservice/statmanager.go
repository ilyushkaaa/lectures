package main

import (
	"sync"
	"time"
)

type StatManager struct {
	mu                    *sync.Mutex
	stat                  map[int]*Stat
	currentStatConsumerID int
}

func NewStatManager() *StatManager {
	return &StatManager{
		mu:   &sync.Mutex{},
		stat: map[int]*Stat{},
	}
}

func (sm *StatManager) UpdateStat(consumer, method string) {
	sm.mu.Lock()
	defer sm.mu.Unlock()
	for _, currentStat := range sm.stat {
		value := currentStat.ByConsumer[consumer]
		value++
		currentStat.ByConsumer[consumer] = value
		value = currentStat.ByMethod[method]
		value++
		currentStat.ByMethod[method] = value
		currentStat.Timestamp = time.Now().Unix()
	}
}

func (sm *StatManager) AddStatConsumer() int {
	sm.mu.Lock()
	defer sm.mu.Unlock()
	sm.currentStatConsumerID++
	sm.stat[sm.currentStatConsumerID] = &Stat{
		ByMethod:   map[string]uint64{},
		ByConsumer: map[string]uint64{},
	}
	return sm.currentStatConsumerID
}

func (sm *StatManager) GetStat(statConsumerID int) *Stat {
	sm.mu.Lock()
	defer func() {
		sm.stat[statConsumerID] = &Stat{
			ByMethod:   map[string]uint64{},
			ByConsumer: map[string]uint64{},
		}
		sm.mu.Unlock()
	}()
	statToReturn := sm.stat[statConsumerID]
	return statToReturn
}
