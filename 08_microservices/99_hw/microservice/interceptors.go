package main

import (
	"context"
	"github.com/casbin/casbin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

func (sm *ServiceManager) authStreamInterceptor(
	srv interface{},
	stream grpc.ServerStream,
	info *grpc.StreamServerInfo,
	handler grpc.StreamHandler,
) error {
	consumer, err := checkAuth(stream.Context(), sm.Enforcer, info.FullMethod)
	if err != nil {
		return err
	}
	sm.AdminMan.HandleUpdate(consumer, info.FullMethod)
	return handler(srv, stream)
}

func (sm *ServiceManager) authInterceptor(
	ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler,
) (interface{}, error) {
	consumer, err := checkAuth(ctx, sm.Enforcer, info.FullMethod)
	if err != nil {
		return nil, err
	}
	sm.AdminMan.HandleUpdate(consumer, info.FullMethod)
	reply, err := handler(ctx, req)
	return reply, err
}

func getConsumerFromContext(ctx context.Context) (string, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return "", status.Errorf(codes.Unauthenticated, "no metadata in context")
	}
	consumers := md.Get("consumer")
	if len(consumers) == 0 {
		return "", status.Errorf(codes.Unauthenticated, "no consumer in context")
	}
	return consumers[0], nil
}

func checkAuth(ctx context.Context, enforcer *casbin.Enforcer, fullMethod string) (string, error) {
	consumer, err := getConsumerFromContext(ctx)
	if err != nil {
		return "", status.Errorf(codes.Unauthenticated, "cant get consumer from context")
	}
	res, err := enforcer.EnforceSafe(consumer, fullMethod)
	if !res || err != nil {
		return "", status.Errorf(codes.Unauthenticated, "forbidden resource")
	}
	return consumer, nil
}
