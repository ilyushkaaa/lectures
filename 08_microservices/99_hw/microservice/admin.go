package main

import (
	"errors"
	"google.golang.org/grpc/peer"
	"sync"
	"time"
)

type AdminManager struct {
	UnimplementedAdminServer
	mu      *sync.Mutex
	LogMan  *LogManager
	StatMan *StatManager
}

func NewAdminManager() *AdminManager {
	return &AdminManager{
		mu:      &sync.Mutex{},
		LogMan:  NewLogManager(),
		StatMan: NewStatManager(),
	}
}

func (am *AdminManager) Logging(_ *Nothing, logStream Admin_LoggingServer) error {
	peerInfo, ok := peer.FromContext(logStream.Context())
	if !ok {
		return errors.New("failed to get client address")
	}
	events, key := am.LogMan.AddChanel(peerInfo.Addr.String())
	defer am.LogMan.DeleteChannel(key)
	for {
		select {
		case <-logStream.Context().Done():
			return nil
		case newEvent := <-events:
			if err := logStream.Send(newEvent); err != nil {
				return err
			}
		}
	}
}

func (am *AdminManager) Statistics(statInterval *StatInterval, statisticStream Admin_StatisticsServer) error {
	statConsumerID := am.StatMan.AddStatConsumer()
	ticker := time.NewTicker(time.Second * time.Duration(statInterval.IntervalSeconds))
	defer ticker.Stop()
	for {
		select {
		case <-statisticStream.Context().Done():
			return nil
		case <-ticker.C:
			if err := statisticStream.Send(am.StatMan.GetStat(statConsumerID)); err != nil {
				return err
			}

		}
	}
}

func (am *AdminManager) HandleUpdate(consumer, method string) {
	am.LogMan.AddLog(consumer, method)
	am.StatMan.UpdateStat(consumer, method)
}
