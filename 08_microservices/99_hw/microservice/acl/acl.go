package acl

import (
	"encoding/csv"
	"encoding/json"
	"log"
	"os"
)

func ParseACLData(aclData string) (map[string][]string, error) {
	parsedACL := map[string][]string{}
	err := json.Unmarshal([]byte(aclData), &parsedACL)
	if err != nil {
		return nil, err
	}
	return parsedACL, nil

}

func CreateACLFile(aclData map[string][]string) error {
	file, err := os.Create("acl/acl_data.csv")
	if err != nil {
		return err
	}
	defer func(file *os.File) {
		err = file.Close()
		if err != nil {
			log.Printf("error in file closing")
		}
	}(file)
	writer := csv.NewWriter(file)
	defer writer.Flush()
	for role, paths := range aclData {
		for _, path := range paths {
			err = writer.Write([]string{"p", role, path})
			if err != nil {
				return err
			}
		}
	}
	return nil
}
