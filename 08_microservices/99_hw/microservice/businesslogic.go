package main

import "context"

type BusinessLogicManager struct {
	UnimplementedBizServer
}

func NewBusinessLogicManager() *BusinessLogicManager {
	return &BusinessLogicManager{}
}

func (blm *BusinessLogicManager) Check(_ context.Context, n *Nothing) (*Nothing, error) {
	return n, nil
}

func (blm *BusinessLogicManager) Add(_ context.Context, n *Nothing) (*Nothing, error) {
	return n, nil
}

func (blm *BusinessLogicManager) Test(_ context.Context, n *Nothing) (*Nothing, error) {
	return n, nil
}
