package main

import (
	"context"
	"github.com/casbin/casbin"
	acl "gitlab.com/vk-golang/lectures/08_microservices/99_hw/microservice/acl"
	"google.golang.org/grpc"
	"log"
	"net"
)

// тут вы пишете код
// обращаю ваше внимание - в этом задании запрещены глобальные переменные
// если хочется, то для красоты можно разнести логику по разным файликам

func StartMyMicroservice(ctx context.Context, addr string, aclData string) error {
	parsedACL, err := acl.ParseACLData(aclData)
	if err != nil {
		return err
	}
	err = acl.CreateACLFile(parsedACL)
	if err != nil {
		return err
	}
	enforcer, err := casbin.NewEnforcerSafe("acl/acl_policy.conf", "acl/acl_data.csv")
	if err != nil {
		return err
	}
	adminMan := NewAdminManager()
	serviceManager := &ServiceManager{
		Enforcer: enforcer,
		AdminMan: adminMan,
	}
	listener, err := initServer(serviceManager, addr)
	if err != nil {
		return err
	}
	go func() {
		<-ctx.Done()
		err = listener.Close()
		if err != nil {
			log.Printf("cant close listener: %s", err)
		}
	}()
	return nil

}

func initServer(sm *ServiceManager, addr string) (net.Listener, error) {
	listener, err := net.Listen("tcp", addr)
	if err != nil {
		return nil, err
	}

	server := grpc.NewServer(
		grpc.UnaryInterceptor(sm.authInterceptor),
		grpc.StreamInterceptor(sm.authStreamInterceptor),
	)
	RegisterAdminServer(server, sm.AdminMan)
	RegisterBizServer(server, NewBusinessLogicManager())
	go func() {
		err = server.Serve(listener)
		if err != nil {
			return
		}
	}()
	return listener, nil
}

type ServiceManager struct {
	Enforcer *casbin.Enforcer
	AdminMan *AdminManager
}
