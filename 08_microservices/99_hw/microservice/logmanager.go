package main

import (
	"strconv"
	"strings"
	"sync"
	"time"
)

type LogManager struct {
	mu             *sync.Mutex
	streamChannels map[string]chan *Event
	streamCounter  int
}

func NewLogManager() *LogManager {
	return &LogManager{
		streamChannels: make(map[string]chan *Event),
		mu:             &sync.Mutex{},
	}
}

func (lm *LogManager) AddLog(consumer, method string) {
	lm.mu.Lock()
	defer lm.mu.Unlock()
	for addr, currentStreamChan := range lm.streamChannels {
		currentStreamChan <- &Event{
			Timestamp: time.Now().Unix(),
			Consumer:  consumer,
			Method:    method,
			Host:      strings.Split(addr, " ")[0],
		}
	}
}

func (lm *LogManager) AddChanel(clientAddr string) (chan *Event, string) {
	lm.mu.Lock()
	defer lm.mu.Unlock()
	events := make(chan *Event)
	lm.streamCounter++
	key := clientAddr + " " + strconv.Itoa(lm.streamCounter)
	lm.streamChannels[key] = events
	return events, key
}

func (lm *LogManager) DeleteChannel(key string) {
	lm.mu.Lock()
	defer lm.mu.Unlock()
	close(lm.streamChannels[key])
	delete(lm.streamChannels, key)

}
