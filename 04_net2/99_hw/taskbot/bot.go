package main

// сюда писать код

import (
	"context"
	"fmt"
	tgbotapi "github.com/skinass/telegram-bot-api/v5"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
)

var (
	BotToken   = "6627237763:AAH_kLL9NCmMRB3xxV1mKLio7ZJLhDPVxlA"
	WebhookURL = "https://145f-79-104-6-133.ngrok.io"
)

var (
	ActionFunctions           map[string]func(update tgbotapi.Update) map[int64]string
	Actions                   []Action
	regexpForChangeState      *regexp.Regexp
	TaskManagerForThisSession TaskManager
	UserManagerForThisSession UserManager
)

const unknownCommand = "Unknown command"

type TaskManager struct {
	mu         *sync.RWMutex
	Tasks      []*Task
	NextTaskID int
}

type User struct {
	ID            int64
	UserName      string
	AssignedTasks []*Task
}

type UserManager struct {
	mu    *sync.RWMutex
	Users map[int64]User
}

type Task struct {
	ID         int
	TaskText   string
	CreatorID  int64
	IsAssigned bool
}

type Action struct {
	Name                 string
	IsAssignmentRequired bool
	IsChangeStateAction  bool
}

func initTaskManager() TaskManager {
	return TaskManager{
		Tasks:      make([]*Task, 0),
		NextTaskID: 1,
		mu:         &sync.RWMutex{},
	}
}

func initUserManager() UserManager {
	return UserManager{
		mu:    &sync.RWMutex{},
		Users: initUsers(),
	}
}

func initActions() []Action {
	actions := make([]Action, 0, 7)
	actions = append(actions, Action{
		Name:                 "/tasks",
		IsAssignmentRequired: false,
		IsChangeStateAction:  false,
	})
	actions = append(actions, Action{
		Name:                 "/new",
		IsAssignmentRequired: false,
		IsChangeStateAction:  false,
	})
	actions = append(actions, Action{
		Name:                 "/assign",
		IsAssignmentRequired: false,
		IsChangeStateAction:  true,
	})
	actions = append(actions, Action{
		Name:                 "/unassign",
		IsAssignmentRequired: true,
		IsChangeStateAction:  true,
	})
	actions = append(actions, Action{
		Name:                 "/resolve",
		IsAssignmentRequired: true,
		IsChangeStateAction:  true,
	})
	actions = append(actions, Action{
		Name:                 "/my",
		IsAssignmentRequired: false,
		IsChangeStateAction:  false,
	})
	actions = append(actions, Action{
		Name:                 "/owner",
		IsAssignmentRequired: false,
		IsChangeStateAction:  false,
	})
	return actions

}
func initUsers() map[int64]User {
	return make(map[int64]User)
}

func initActionFunc() map[string]func(update tgbotapi.Update) map[int64]string {
	actionsToInit := make(map[string]func(update tgbotapi.Update) map[int64]string, 7)
	actionsToInit["/tasks"] = HandleMessageTasks
	actionsToInit["/new"] = HandleMessageNew
	actionsToInit["/assign"] = HandleMessageAssign
	actionsToInit["/unassign"] = HandleMessageUnAssign
	actionsToInit["/resolve"] = HandleMessageResolve
	actionsToInit["/my"] = HandleMessageMy
	actionsToInit["/owner"] = HandleMessageOwner
	return actionsToInit
}

func (task *Task) GetAssignee(userID int64) string { // получает того, на кого назначена конкретная задача
	UserManagerForThisSession.mu.RLock()
	defer UserManagerForThisSession.mu.RUnlock()
	if task.IsUserAssignee(userID) {
		return "я"
	}
	for _, u := range UserManagerForThisSession.Users {
		if task.IsUserAssignee(u.ID) {
			return "@" + u.UserName
		}
	}
	return ""
}

func getAssigneeMessage(assignee string) string {
	return "assignee: " + assignee
}
func (task *Task) makeNewMessage() string {
	return fmt.Sprintf(`Задача "%s" создана, id=%d`, task.TaskText, task.ID)
}

func addUser(id int64, userName string) {
	UserManagerForThisSession.mu.Lock()
	defer UserManagerForThisSession.mu.Unlock()
	newUser := User{
		ID:            id,
		AssignedTasks: []*Task{},
		UserName:      userName,
	}
	UserManagerForThisSession.Users[id] = newUser
}

func AddTask(taskText string, creatorID int64) *Task {
	TaskManagerForThisSession.mu.Lock()
	defer TaskManagerForThisSession.mu.Unlock()
	newTask := &Task{
		ID:        TaskManagerForThisSession.NextTaskID,
		TaskText:  taskText,
		CreatorID: creatorID,
	}
	TaskManagerForThisSession.NextTaskID++
	TaskManagerForThisSession.Tasks = append(TaskManagerForThisSession.Tasks, newTask)
	return newTask
}

func (task *Task) IsUserAssignee(userID int64) bool {
	UserManagerForThisSession.mu.RLock()
	defer UserManagerForThisSession.mu.RUnlock()
	for _, t := range UserManagerForThisSession.Users[userID].AssignedTasks {
		if task.ID == t.ID {
			return true
		}
	}
	return false
}

func (task *Task) getInfo() string {
	return fmt.Sprintf("%d. %s by @%s", task.ID, task.TaskText, UserManagerForThisSession.Users[task.CreatorID].UserName)
}

func makeUnknownCommandMessage(id int64) map[int64]string {
	return map[int64]string{
		id: unknownCommand,
	}
}

func (task *Task) GetAvailableActions(userID int64) string {
	var availableActions []string
	if !task.IsAssigned {
		for _, act := range Actions {
			if !act.IsAssignmentRequired && act.IsChangeStateAction {
				availableActions = append(availableActions, fmt.Sprintf("%s_%d", act.Name, task.ID))
			}
		}
	}
	if task.IsUserAssignee(userID) {
		for _, act := range Actions {
			if act.IsAssignmentRequired && act.IsChangeStateAction {
				availableActions = append(availableActions, fmt.Sprintf("%s_%d", act.Name, task.ID))
			}
		}
	}
	result := strings.Join(availableActions, " ")
	if result != "" {
		result = "\n" + result
	}
	return result
}

func (task *Task) GetPreviousAssigneeID() (int64, error) {
	UserManagerForThisSession.mu.RLock()
	defer UserManagerForThisSession.mu.RUnlock()
	for id := range UserManagerForThisSession.Users {
		if task.IsUserAssignee(id) {
			return id, nil
		}
	}
	return 0, fmt.Errorf("no previous user")
}

func GetParamValue(messageText string) (int, error) {
	params := strings.Split(messageText, "_")
	if len(params) != 2 {
		return 0, fmt.Errorf("bad number of params")
	}
	numTask, err := strconv.Atoi(params[1])
	if err != nil {
		return 0, fmt.Errorf("bad param value")
	}
	return numTask, nil
}

func (task *Task) AddForUser(user User) {
	UserManagerForThisSession.mu.Lock()
	defer UserManagerForThisSession.mu.Unlock()
	newUser := UserManagerForThisSession.Users[user.ID]
	newUser.AssignedTasks = append(newUser.AssignedTasks, task)
	UserManagerForThisSession.Users[user.ID] = newUser
}

func DeleteTaskFromOldAssigner(user User, indexToDelete int) {
	newUser := UserManagerForThisSession.Users[user.ID]
	newUser.AssignedTasks = append(newUser.AssignedTasks[:indexToDelete],
		newUser.AssignedTasks[indexToDelete+1:]...)
	UserManagerForThisSession.Users[user.ID] = newUser
}

func HandleUpdate(update tgbotapi.Update) map[int64]string {
	messageText := update.Message.Text
	log.Println("MESSAGE: ", messageText)
	var funcToCall func(update tgbotapi.Update) map[int64]string
	var ok bool
	if regexpForChangeState.MatchString(messageText) {
		indexOf_ := strings.Index(messageText, "_")
		funcToCall, ok = ActionFunctions[messageText[:indexOf_]]
	} else {
		messageWords := strings.Split(messageText, " ")
		funcToCall, ok = ActionFunctions[messageWords[0]]

	}
	if !ok {
		return map[int64]string{
			update.Message.Chat.ID: unknownCommand,
		}
	}
	return funcToCall(update)
}

func HandleMessageTasks(update tgbotapi.Update) map[int64]string {
	if update.Message.Text != "/tasks" {
		return makeUnknownCommandMessage(update.Message.Chat.ID)
	}
	answer := make(map[int64]string)
	answersForEachTask := make([]string, 0)
	TaskManagerForThisSession.mu.RLock()
	for _, task := range TaskManagerForThisSession.Tasks {
		answerForCurrentTask := task.getInfo()
		taskAssignee := task.GetAssignee(update.Message.Chat.ID)
		if taskAssignee != "" {
			answerForCurrentTask += "\n" + getAssigneeMessage(taskAssignee)
		}
		answerForCurrentTask += task.GetAvailableActions(update.Message.Chat.ID)
		answersForEachTask = append(answersForEachTask, answerForCurrentTask)
	}
	TaskManagerForThisSession.mu.RUnlock()
	if len(answersForEachTask) == 0 {
		answersForEachTask = append(answersForEachTask, "Нет задач")
	}
	answer[update.Message.Chat.ID] = strings.Join(answersForEachTask, "\n\n")
	return answer
}

func HandleMessageNew(update tgbotapi.Update) map[int64]string {
	params := strings.Split(update.Message.Text, " ")
	if len(params) < 2 {
		return map[int64]string{update.Message.Chat.ID: "Wrong param number"}
	}
	params = params[1:]
	taskText := strings.Join(params, " ")
	newTask := AddTask(taskText, update.Message.Chat.ID)
	return map[int64]string{update.Message.Chat.ID: newTask.makeNewMessage()}
}

func HandleMessageAssign(update tgbotapi.Update) map[int64]string {
	messageText := update.Message.Text
	numTask, err := GetParamValue(messageText)
	if err != nil {
		return map[int64]string{update.Message.Chat.ID: err.Error()}
	}
	answers := make(map[int64]string)

	TaskManagerForThisSession.mu.RLock()
	for _, task := range TaskManagerForThisSession.Tasks {
		if task.ID != numTask {
			continue
		}
		task.IsAssigned = true
		answers[update.Message.Chat.ID] = fmt.Sprintf(`Задача "%s" назначена на вас`, task.TaskText)
		if prevUID, err := task.GetPreviousAssigneeID(); err == nil {
			UserManagerForThisSession.mu.Lock()
			answers[prevUID] = fmt.Sprintf(`Задача "%s" назначена на @%s`, task.TaskText,
				UserManagerForThisSession.Users[update.Message.Chat.ID].UserName)
			for i, task := range UserManagerForThisSession.Users[prevUID].AssignedTasks {
				if task.ID == numTask {
					DeleteTaskFromOldAssigner(UserManagerForThisSession.Users[prevUID], i)
				}
			}
			UserManagerForThisSession.mu.Unlock()
		} else if task.CreatorID != update.Message.Chat.ID {
			UserManagerForThisSession.mu.RLock()
			answers[task.CreatorID] = fmt.Sprintf(`Задача "%s" назначена на @%s`, task.TaskText,
				UserManagerForThisSession.Users[update.Message.Chat.ID].UserName)
			UserManagerForThisSession.mu.RUnlock()

		}
		task.AddForUser(UserManagerForThisSession.Users[update.Message.Chat.ID])
		break

	}
	TaskManagerForThisSession.mu.RUnlock()
	if len(answers) == 0 {
		answers[update.Message.Chat.ID] = fmt.Sprintf(`Задачи с id=%d не существует`, numTask)
	}
	return answers
}

func HandleMessageUnAssign(update tgbotapi.Update) map[int64]string {
	messageText := update.Message.Text
	numTask, err := GetParamValue(messageText)
	if err != nil {
		return map[int64]string{update.Message.Chat.ID: err.Error()}
	}
	answers := make(map[int64]string)
	UserManagerForThisSession.mu.Lock()
	for i, task := range UserManagerForThisSession.Users[update.Message.Chat.ID].AssignedTasks {
		if task.ID != numTask {
			continue
		}
		task.IsAssigned = false
		answers[update.Message.Chat.ID] = "Принято"
		DeleteTaskFromOldAssigner(UserManagerForThisSession.Users[update.Message.Chat.ID], i)
		answers[task.CreatorID] = fmt.Sprintf(`Задача "%s" осталась без исполнителя`, task.TaskText)
		break
	}
	UserManagerForThisSession.mu.Unlock()

	if len(answers) == 0 {
		answers[update.Message.Chat.ID] = "Задача не на вас"
	}
	return answers
}

func HandleMessageResolve(update tgbotapi.Update) map[int64]string {
	messageText := update.Message.Text
	numTask, err := GetParamValue(messageText)
	if err != nil {
		return map[int64]string{update.Message.Chat.ID: err.Error()}
	}
	answers := make(map[int64]string)
	UserManagerForThisSession.mu.Lock()
	for i, task := range UserManagerForThisSession.Users[update.Message.Chat.ID].AssignedTasks {
		if task.ID != numTask {
			continue
		}
		answers[update.Message.Chat.ID] = fmt.Sprintf(`Задача "%s" выполнена`, task.TaskText)
		DeleteTaskFromOldAssigner(UserManagerForThisSession.Users[update.Message.Chat.ID], i)
		answers[task.CreatorID] = fmt.Sprintf(`Задача "%s" выполнена @%s`, task.TaskText,
			UserManagerForThisSession.Users[update.Message.Chat.ID].UserName)
		break
	}
	UserManagerForThisSession.mu.Unlock()
	TaskManagerForThisSession.mu.Lock()
	for i, task := range TaskManagerForThisSession.Tasks {
		if task.ID != numTask {
			continue
		}
		TaskManagerForThisSession.Tasks = append(TaskManagerForThisSession.Tasks[:i], TaskManagerForThisSession.Tasks[i+1:]...)
		break
	}
	TaskManagerForThisSession.mu.Unlock()
	if len(answers) == 0 {
		answers[update.Message.Chat.ID] = "Задача не на вас"
	}
	return answers
}

func HandleMessageMy(update tgbotapi.Update) map[int64]string {
	if update.Message.Text != "/my" {
		return makeUnknownCommandMessage(update.Message.Chat.ID)
	}
	answer := make(map[int64]string)
	answersForEachTask := make([]string, 0)
	UserManagerForThisSession.mu.RLock()
	for _, task := range UserManagerForThisSession.Users[update.Message.Chat.ID].AssignedTasks {
		answerForCurrentTask := task.getInfo()
		answerForCurrentTask += task.GetAvailableActions(update.Message.Chat.ID)
		answersForEachTask = append(answersForEachTask, answerForCurrentTask)
	}
	UserManagerForThisSession.mu.RUnlock()
	if len(answersForEachTask) == 0 {
		answersForEachTask = append(answersForEachTask, "На вас задачи не назначены")
	}
	answer[update.Message.Chat.ID] = strings.Join(answersForEachTask, "\n")
	return answer
}

func HandleMessageOwner(update tgbotapi.Update) map[int64]string {
	if update.Message.Text != "/owner" {
		return makeUnknownCommandMessage(update.Message.Chat.ID)
	}
	answer := make(map[int64]string)
	answersForEachTask := make([]string, 0)
	TaskManagerForThisSession.mu.RLock()
	for _, task := range TaskManagerForThisSession.Tasks {
		if task.CreatorID != update.Message.Chat.ID {
			continue
		}
		answerForCurrentTask := task.getInfo()
		answerForCurrentTask += task.GetAvailableActions(update.Message.Chat.ID)
		answersForEachTask = append(answersForEachTask, answerForCurrentTask)
	}
	TaskManagerForThisSession.mu.RUnlock()
	if len(answersForEachTask) == 0 {
		answersForEachTask = append(answersForEachTask, "Вы задачи не назначали")
	}
	answer[update.Message.Chat.ID] = strings.Join(answersForEachTask, "\n")
	return answer
}
func startWebhookForTest() {
	http.ListenAndServe(":8081", nil) // nolint:errcheck
}

func startTaskBot(ctx context.Context) error {
	bot, err := tgbotapi.NewBotAPI(BotToken)
	if err != nil {
		return err
	}
	wh, err := tgbotapi.NewWebhook(WebhookURL)
	if err != nil {
		return err
	}
	_, err = bot.Request(wh)
	if err != nil {
		return err
	}
	ActionFunctions = initActionFunc()
	TaskManagerForThisSession = initTaskManager()
	Actions = initActions()
	UserManagerForThisSession = initUserManager()
	updates := bot.ListenForWebhook("/")
	regexpForChangeState = regexp.MustCompile("/[a-z]+_[0-9]+")
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	go func() {
		http.ListenAndServe(":"+port, nil) // nolint:errcheck
	}()
	if BotToken == "_golangcourse_test" {
		go startWebhookForTest()
	}
	for {
		select {
		case <-ctx.Done():
			return nil
		case update := <-updates:
			if _, ok := UserManagerForThisSession.Users[update.Message.Chat.ID]; !ok {
				addUser(update.Message.Chat.ID, update.Message.Chat.UserName)
			}
			go func(update tgbotapi.Update) {
				answer := HandleUpdate(update)
				for chatID, message := range answer {
					msg := tgbotapi.NewMessage(chatID, message)
					_, botSendError := bot.Send(msg)
					if botSendError != nil {
						return
					}
				}
			}(update)

		}
	}
}

func main() {
	err := startTaskBot(context.Background())
	if err != nil {
		panic(err)
	}
}
