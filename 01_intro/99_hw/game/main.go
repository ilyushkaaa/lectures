package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

/*
	код писать в этом файле
	наверняка у вас будут какие-то структуры с методами, глобальные переменные ( тут можно ), функции
*/

var locations []*Location
var player Player
var actions map[string]Action
var subjects []Subject
var actionsForAppliances map[string]func() string
var isDoorBetweenRoomsOpen map[string]bool
var targetsFunctions []func() bool
var targetNames []string

type Furniture struct {
	subjects []Subject
	name     string
}

type Player struct {
	items    map[string]bool
	location *Location
}

type Action struct {
	functionForAction func(string, string) string
	messageForSuccess string
}

func newAction(functionForAction func(string, string) string,
	messageForSuccess string) Action {
	return Action{functionForAction,
		messageForSuccess}
}

type Subject struct {
	name                            string
	requiredAnotherSubjects         []Subject
	subjectsForWhichItCanBeUsed     []string
	availableActionsWithThisSubject map[string]bool
}

type Location struct {
	descriptionForMove              string
	descriptionForLookAround        string
	name                            string
	furnitureOfThisLocation         []Furniture
	isCloseLocationAtStartOfTheGame bool
	adjoiningLocations              []*Location
	afterSubjectsFieldDelimiter     string
	needWriteTarget                 bool
}

func newPlayer() Player {
	var newPl Player
	newPl.items = map[string]bool{}
	location := &locations[0]
	newPl.location = *location
	return newPl

}
func newFurniture(subjects []Subject, name string) Furniture {
	return Furniture{subjects, name}
}

func newSubject(name string, requiredAnotherSubjects []Subject, subjectsForWhichItCanBeUsed []string,
	availableActionsWithThisSubject map[string]bool) (sub Subject) {
	sub = Subject{name, requiredAnotherSubjects,
		subjectsForWhichItCanBeUsed, availableActionsWithThisSubject}
	return
}

func newLocation(descriptionForMove string, descriptionForLookAround string, name string, furniture []Furniture,
	isCloseLocationAtStartOfTheGame bool, adjoiningLocations []*Location, afterSubjectsFieldDelimiter string, needWriteTarget bool) *Location {
	newLoc := Location{descriptionForMove, descriptionForLookAround,
		name, furniture, isCloseLocationAtStartOfTheGame,
		adjoiningLocations,
		afterSubjectsFieldDelimiter, needWriteTarget}
	for _, neighbourForCurrentLocation := range newLoc.adjoiningLocations {
		keyForDoor := newLoc.name + neighbourForCurrentLocation.name
		if neighbourForCurrentLocation.isCloseLocationAtStartOfTheGame ||
			newLoc.isCloseLocationAtStartOfTheGame {
			isDoorBetweenRoomsOpen[keyForDoor] = false
		} else {
			isDoorBetweenRoomsOpen[keyForDoor] = true
		}

	}
	return &newLoc
}

func openCloseDoor() string {
	if player.location.name == "коридор" {
		if isDoorBetweenRoomsOpen[player.location.name+locations[3].name] {
			isDoorBetweenRoomsOpen[player.location.name+locations[3].name] = false
			isDoorBetweenRoomsOpen[locations[3].name+player.location.name] = false
			return "дверь закрыта"
		} else {
			isDoorBetweenRoomsOpen[player.location.name+locations[3].name] = true
			isDoorBetweenRoomsOpen[locations[3].name+player.location.name] = true
			return "дверь открыта"
		}

	}
	return "нет двери в этой комнате"

}

func checkIfActionMayBeWithThisSubject(action string, subject Subject) string {
	if !subject.availableActionsWithThisSubject[action] {
		return "Нельзя " + action + " " + subject.name
	}
	return ""

}

func lookAround(_, _ string) string {
	var subjectsField string
	var target = ""

	if player.location.needWriteTarget {
		var indexesOfTargets []int
		for index, need := range targetsFunctions {
			if need() {
				indexesOfTargets = append(indexesOfTargets, index)
			}
		}
		for i, targetFromList := range indexesOfTargets {
			if i == 0 {
				target = "надо " + targetNames[targetFromList]
			} else {
				target += " и " + targetNames[targetFromList]
			}
		}

	}
	if target != "" {
		target += ". "
	}
	var subjectsFieldForAllFurniture []string
	for _, furn := range player.location.furnitureOfThisLocation {
		subjectsFieldForThisFurniture := ""
		for j, sub := range furn.subjects {
			if j == 0 {
				subjectsFieldForThisFurniture += "на " + furn.name + "е: " + sub.name
			} else {
				subjectsFieldForThisFurniture += ", " + sub.name
			}
		}
		if subjectsFieldForThisFurniture != "" {
			subjectsFieldForAllFurniture = append(subjectsFieldForAllFurniture, subjectsFieldForThisFurniture)
		}

	}
	subjectsField += strings.Join(subjectsFieldForAllFurniture, ", ")
	if subjectsField != "" {
		subjectsField += player.location.afterSubjectsFieldDelimiter
	} else {
		subjectsField = "пустая комната. "
	}
	canGo := playerCanGo()
	return player.location.descriptionForLookAround + subjectsField + target + canGo

}

func playerCanGo() (canGo string) {
	locationsToGo := make([]string, 0, 1)
	for _, loc := range player.location.adjoiningLocations {
		locationsToGo = append(locationsToGo, loc.name)
	}
	listOfLocations := strings.Join(locationsToGo, ", ")
	canGo = "можно пройти - " + listOfLocations
	return
}

func move(destination, _ string) string {
	availableLocations := map[string]bool{}
	var description string
	for _, loc := range player.location.adjoiningLocations {
		availableLocations[loc.name] = true
	}
	if !availableLocations[destination] {
		return "нет пути в " + destination
	} else {
		for i := range locations {
			if locations[i].name == destination {
				if isDoorBetweenRoomsOpen[player.location.name+locations[i].name] {
					player.location = locations[i]
					description = locations[i].descriptionForMove
					break
				} else {
					return "дверь закрыта"
				}
			}
		}

	}
	canGo := playerCanGo()
	return description + canGo

}

func putOn(item, _ string) string {
	for i, currentFurniture := range player.location.furnitureOfThisLocation {
		for j, currentSubject := range currentFurniture.subjects {
			if currentSubject.name == item {
				if isThisActionValidString := checkIfActionMayBeWithThisSubject("надеть", currentSubject); isThisActionValidString != "" {
					return isThisActionValidString
				}
				player.location.furnitureOfThisLocation[i].subjects =
					append(player.location.furnitureOfThisLocation[i].subjects[:j],
						player.location.furnitureOfThisLocation[i].subjects[j+1:]...)
				player.items[item] = true
				return actions["надеть"].messageForSuccess + item

			}
		}
	}
	return "нет такого"

}

func take(item, _ string) string {
	for i, currentFurniture := range player.location.furnitureOfThisLocation {
		for j, currentSubject := range currentFurniture.subjects {
			if currentSubject.name == item {
				if isThisActionValidString := checkIfActionMayBeWithThisSubject("взять", currentSubject); isThisActionValidString != "" {
					return isThisActionValidString
				}
				for _, requiredItemsForThis := range currentSubject.requiredAnotherSubjects {
					if exists := player.items[requiredItemsForThis.name]; !exists {
						return "некуда класть"
					}
				}
				player.location.furnitureOfThisLocation[i].subjects =
					append(player.location.furnitureOfThisLocation[i].subjects[:j],
						player.location.furnitureOfThisLocation[i].subjects[j+1:]...)
				player.items[item] = true
				return actions["взять"].messageForSuccess + item

			}
		}
	}
	return "нет такого"

}

func use(what, forWhat string) string {

	if !player.items[what] {
		return "нет предмета в инвентаре - " + what
	}
	for _, currentSubject := range subjects {
		if currentSubject.name == what {
			for _, applianceForCurrentSubject := range currentSubject.subjectsForWhichItCanBeUsed {
				if forWhat == applianceForCurrentSubject {
					return actionsForAppliances[forWhat]()

				}
			}
		}
		return "не к чему применить"

	}

	return "не к чему применить"

}

func needPickUpBag() bool {
	return !(player.items["рюкзак"] && player.items["конспекты"] && player.items["ключи"])
}
func needGoToUniversity() bool {
	return player.location.name != locations[3].name
}

func main() {
	initGame()
	reader := bufio.NewReader(os.Stdin)
	fmt.Println(handleCommand("осмотреться"))
	for player.location.name != "улица" {
		fmt.Print("Введите команду: ")
		if input, err := reader.ReadString('\n'); err == nil {
			input = strings.TrimSpace(input)
			fmt.Println("Ответ: ", handleCommand(input))
		} else {
			fmt.Println("Ошибка")
		}

	}
	fmt.Println("Игра окончена!")
}
func initDoors() map[string]bool {
	doorsOpen := map[string]bool{}
	for _, currentLocation := range locations {
		for _, neighbourForCurrentLocation := range currentLocation.adjoiningLocations {
			keyForDoor := currentLocation.name + neighbourForCurrentLocation.name
			if neighbourForCurrentLocation.isCloseLocationAtStartOfTheGame ||
				currentLocation.isCloseLocationAtStartOfTheGame {
				doorsOpen[keyForDoor] = false
			} else {
				doorsOpen[keyForDoor] = true
			}

		}
	}
	return doorsOpen

}

func initTargets() {
	targetNames = []string{}
	targetsFunctions = []func() bool{}
	targetNames = append(targetNames, "собрать рюкзак", "идти в универ")
	targetsFunctions = append(targetsFunctions, needPickUpBag, needGoToUniversity)
}

func initAppliances() map[string]func() string {
	action := map[string]func() string{}
	action["дверь"] = openCloseDoor
	return action
}

func initLocations() []*Location {
	var locs []*Location
	defaultDelimiter := ". "
	var furnitureForKitchen []Furniture
	var furnitureForRoom []Furniture
	furnitureForKitchen = append(furnitureForKitchen, newFurniture([]Subject{subjects[2]}, "стол"))
	furnitureForRoom = append(furnitureForRoom, newFurniture([]Subject{subjects[0], subjects[3]}, "стол"))
	furnitureForRoom = append(furnitureForRoom, newFurniture([]Subject{subjects[1]}, "стул"))

	locs = append(locs, newLocation("кухня, ничего интересного. ",
		"ты находишься на кухне, ",
		"кухня", furnitureForKitchen, false, []*Location{},
		", ", true))
	locs = append(locs, newLocation("ничего интересного. ", "ты в коридоре, ",
		"коридор", []Furniture{}, false, []*Location{}, defaultDelimiter, false))
	locs = append(locs, newLocation("ты в своей комнате. ", "", "комната",
		furnitureForRoom,
		false, []*Location{}, defaultDelimiter, false))
	locs = append(locs, newLocation("на улице весна. ", "",
		"улица", []Furniture{}, true, []*Location{}, defaultDelimiter, false))
	locs = append(locs, newLocation("", "",
		"домой", []Furniture{}, false, []*Location{}, defaultDelimiter, false))

	locs[0].adjoiningLocations = append(locs[0].adjoiningLocations, locs[1])
	locs[1].adjoiningLocations = append(locs[1].adjoiningLocations, locs[0], locs[2], locs[3])
	locs[2].adjoiningLocations = append(locs[2].adjoiningLocations, locs[1])
	locs[3].adjoiningLocations = append(locs[3].adjoiningLocations, locs[4])
	isDoorBetweenRoomsOpen = initDoors()
	return locs
}

func initSubjects() []Subject {
	var subs []Subject
	subs = append(subs, newSubject("ключи", []Subject{}, []string{"дверь"}, map[string]bool{"взять": true}))
	subs = append(subs, newSubject("рюкзак", []Subject{}, []string{}, map[string]bool{"надеть": true}))
	subs = append(subs, newSubject("чай", []Subject{}, []string{}, map[string]bool{"взять": true}))
	subs = append(subs, newSubject("конспекты", []Subject{}, []string{}, map[string]bool{"взять": true}))
	subs[0].requiredAnotherSubjects = append(subs[0].requiredAnotherSubjects, subs[1])
	subs[3].requiredAnotherSubjects = append(subs[3].requiredAnotherSubjects, subs[1])
	return subs
}

func initActions() map[string]Action {
	actionsMap := map[string]Action{}
	actionsMap["осмотреться"] = newAction(lookAround, "")
	actionsMap["идти"] = newAction(move, "")
	actionsMap["взять"] = newAction(take, "предмет добавлен в инвентарь: ")
	actionsMap["надеть"] = newAction(putOn, "вы надели: ")
	actionsMap["применить"] = newAction(use, "")
	return actionsMap
}

func initGame() {
	subjects = initSubjects()
	actions = initActions()
	locations = initLocations()
	player = newPlayer()
	isDoorBetweenRoomsOpen = initDoors()
	actionsForAppliances = initAppliances()
	initTargets()

}

func handleCommand(command string) string {
	if command == "" {
		return "введите команду"
	}
	paramsForCommand := strings.Split(command, " ")
	action, validCommand := actions[paramsForCommand[0]]
	if !validCommand {
		return "неизвестная команда"
	}
	for len(paramsForCommand) < 3 {
		paramsForCommand = append(paramsForCommand, "")
	}
	return action.functionForAction(paramsForCommand[1], paramsForCommand[2])
}
