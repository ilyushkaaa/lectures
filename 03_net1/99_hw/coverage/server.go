package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
	"strings"
)

var PathFile = "dataset.xml"

const defaultLimit = 100

func isOrderFieldValid(orderField string) bool {
	if orderField != "" {
		if orderField != "ID" && orderField != "Age" && orderField != "Name" {
			return false
		}
	}
	return true
}

func isOrderByValid(orderBy string) bool {
	if orderBy != "" {
		if orderBy != "-1" && orderBy != "0" && orderBy != "1" {
			return false
		}
	}
	return true
}

func makeJSONErrorOrderFieldResp() []byte {
	return []byte(`{"Error": "OrderField invalid"}`)
}
func makeJSONErrorOrderByResp() []byte {
	return []byte(`{"Error": "OrderBy invalid"}`)
}

func makeJSONErrorLimitOffset() []byte {
	return []byte(`{"Error": "invalid limit/offset"}`)

}

func writeNegativeResponseForLimitOffset(w http.ResponseWriter) {
	w.WriteHeader(http.StatusBadRequest)
	w.Write(makeJSONErrorLimitOffset()) // nolint:errcheck
}

func parseParam(paramValue string) (int, error) {
	if paramValue != "" {
		paramValueConverted, err := strconv.Atoi(paramValue)
		if err != nil || paramValueConverted < 0 {
			return -1, err
		}
		return paramValueConverted, nil
	}
	return 0, fmt.Errorf("is empty")
}

func filtrUsers(allUsers *Root, fn func(UserXML) bool) []User {
	users := make([]User, 0)
	for _, currentUser := range allUsers.Users {
		if fn(currentUser) {
			users = append(users, makeUserForResponse(currentUser))
		}
	}
	return users

}

type UserXML struct {
	ID        int    `xml:"id"`
	FirstName string `xml:"first_name"`
	LastName  string `xml:"last_name"`
	Age       int    `xml:"age"`
	About     string `xml:"about"`
	Gender    string `xml:"gender"`
}

type UsersForResponse struct {
	users      []User
	orderField string
	orderBy    string
}

func (s *UsersForResponse) Len() int {
	return len(s.users)
}

func (s *UsersForResponse) Less(i, j int) bool {
	if s.orderBy == "-1" {
		i, j = j, i
	}
	switch s.orderField {
	case "ID":
		return s.users[i].ID < s.users[j].ID
	case "Age":
		return s.users[i].Age < s.users[j].Age
	default:
		return s.users[i].Name < s.users[j].Name
	}
}

func (s *UsersForResponse) Swap(i, j int) {
	s.users[i], s.users[j] = s.users[j], s.users[i]
}

type Root struct {
	Users []UserXML `xml:"row"`
}

func makeFullName(firstName, lastName string) string {
	return firstName + " " + lastName
}

func makeUserForResponse(userXML UserXML) (userForResponse User) {
	aboutField := strings.Split(userXML.About, "\n")
	userForResponse = User{
		ID:     userXML.ID,
		Name:   makeFullName(userXML.FirstName, userXML.LastName),
		Age:    userXML.Age,
		About:  aboutField[0],
		Gender: userXML.Gender,
	}
	return
}

func getMin(n1, n2 int) int {
	if n1 > n2 {
		return n2
	}
	return n1
}

func SearchServer(w http.ResponseWriter, r *http.Request) {
	if accessToken := r.Header.Get("AccessToken"); accessToken == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	queryParams := r.URL.Query()

	limitStr := queryParams.Get("limit")
	offsetStr := queryParams.Get("offset")

	limit, err := parseParam(limitStr)
	if err != nil {
		if err.Error() != "is empty" {
			writeNegativeResponseForLimitOffset(w)
			return
		}
		limit = defaultLimit
	}
	offset, err := parseParam(offsetStr)
	if err != nil {
		if err.Error() != "is empty" {
			writeNegativeResponseForLimitOffset(w)
			return
		}
	}
	query := queryParams.Get("query")
	orderField := queryParams.Get("order_field")
	orderBy := queryParams.Get("order_by")
	if !isOrderFieldValid(orderField) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write(makeJSONErrorOrderFieldResp()) // nolint:errcheck
		return
	}
	if !isOrderByValid(orderBy) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write(makeJSONErrorOrderByResp()) // nolint:errcheck
		return
	}
	xmlData, err := ioutil.ReadFile(PathFile)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	allUsers := &Root{}
	xml.Unmarshal(xmlData, allUsers) // nolint:errcheck
	usersForResponse := UsersForResponse{
		orderBy:    orderBy,
		orderField: orderField,
	}
	usersForResponse.users = filtrUsers(allUsers, func(userXML UserXML) bool {
		return strings.Contains(userXML.LastName, query) || strings.Contains(userXML.FirstName, query) ||
			strings.Contains(userXML.About, query)

	})
	sort.Sort(&usersForResponse)
	startIndex := getMin(offset, len(usersForResponse.users)-1)
	endIndex := getMin(startIndex+limit, len(usersForResponse.users))
	usersForResponse.users = usersForResponse.users[startIndex:endIndex]
	usersResponseJSON, _ := json.Marshal(&usersForResponse.users) // nolint:errcheck
	w.WriteHeader(http.StatusOK)
	w.Write(usersResponseJSON) // nolint:errcheck
}
