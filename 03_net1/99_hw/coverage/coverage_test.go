package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"reflect"
	"testing"
	"time"
)

type TestCase struct {
	RequestParams  SearchRequest
	ResponseResult *SearchResponse
	HasError       bool
	ErrorStr       string
}

func SlowSearchServer(_ http.ResponseWriter, _ *http.Request) {
	time.Sleep(2 * time.Second)
}

func BadJSONSearchServer(w http.ResponseWriter, r *http.Request) {
	queryParams := r.URL.Query()
	orderField := queryParams.Get("order_field")
	if !isOrderFieldValid(orderField) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"badJson"`)) // nolint:errcheck
		return
	}
}

func BadJSONResponseSearchServer(w http.ResponseWriter, _ *http.Request) {
	usersResponseJSON := `{"id":1`
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(usersResponseJSON)) // nolint:errcheck
}

func (srv *SearchClient) FindUsersWithOutValidation(limit, offset, query string) (*SearchResponse, error) { // nolint:unparam

	searcherParams := url.Values{}
	searcherParams.Add("limit", limit)
	searcherParams.Add("offset", offset)
	searcherParams.Add("query", query)

	searcherReq, _ := http.NewRequest("GET", srv.URL+"?"+searcherParams.Encode(), nil) // nolint:errcheck
	searcherReq.Header.Add("AccessToken", srv.AccessToken)

	resp, _ := client.Do(searcherReq) // nolint:errcheck

	body, _ := io.ReadAll(resp.Body) //nolint:errcheck
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusBadRequest {
		errResp := SearchErrorResponse{}
		json.Unmarshal(body, &errResp) // nolint:errcheck

		return nil, fmt.Errorf("invalid field: %s", errResp.Error)
	}
	data := []User{}
	json.Unmarshal(body, &data) // nolint:errcheck

	result := SearchResponse{}
	result.Users = data
	return &result, nil

}

type TestcasesForBadLimitAndOffset struct {
	Limit          string
	Offset         string
	Query          string
	ResponseResult *SearchResponse
	HasError       bool
	ErrorStr       string
}

func TestUnknownError(t *testing.T) {
	searchClient := &SearchClient{
		URL:         "",
		AccessToken: "1",
	}
	testCase := TestCase{
		RequestParams: SearchRequest{
			Limit:      1,
			Offset:     0,
			Query:      "on",
			OrderField: "Name",
			OrderBy:    1,
		},
		ResponseResult: nil,
		HasError:       true,
		ErrorStr:       "unknown error Get \"?limit=2&offset=0&order_by=1&order_field=Name&query=on\": unsupported protocol scheme \"\"",
	}
	_, err := searchClient.FindUsers(testCase.RequestParams)
	if err == nil {
		t.Errorf("expected error, got nil")
	} else if err.Error() != testCase.ErrorStr {
		t.Errorf("wrong error: expected %s, got %s", testCase.ErrorStr, err.Error())

	}

}

func TestLimitAndOffset(t *testing.T) {
	testServer := httptest.NewServer(http.HandlerFunc(SearchServer))
	searchClient := &SearchClient{
		URL:         testServer.URL,
		AccessToken: "1",
	}

	testcases := []TestcasesForBadLimitAndOffset{
		{
			Limit:          "q",
			Offset:         "1",
			ResponseResult: nil,
			HasError:       true,
			ErrorStr:       "invalid field: invalid limit/offset",
		},
		{
			Limit:          "2",
			Offset:         "q",
			ResponseResult: nil,
			HasError:       true,
			ErrorStr:       "invalid field: invalid limit/offset",
		},
		{
			Limit:  "",
			Offset: "",
			Query:  "Beulah",
			ResponseResult: &SearchResponse{
				Users: []User{ // пустые переданные лимит и оффсет
					{
						ID:     5,
						Name:   "Beulah Stark",
						Age:    30,
						About:  "Enim cillum eu cillum velit labore. In sint esse nulla occaecat voluptate pariatur aliqua aliqua non officia nulla aliqua. Fugiat nostrud irure officia minim cupidatat laborum ad incididunt dolore. Fugiat nostrud eiusmod ex ea nulla commodo. Reprehenderit sint qui anim non ad id adipisicing qui officia Lorem.",
						Gender: "female",
					},
				},
				NextPage: false,
			},
			HasError: false,
			ErrorStr: "",
		},
	}

	for caseNum, item := range testcases {
		resp, err := searchClient.FindUsersWithOutValidation(item.Limit, item.Offset, item.Query)

		if err != nil {
			if !item.HasError {
				t.Errorf("[%d] unexpected error %s", caseNum, err.Error())
			} else if err.Error() != item.ErrorStr {
				t.Errorf("[%d] wrong error: expected %s, got %s", caseNum, item.ErrorStr, err.Error())

			}

		}
		if err == nil && item.HasError {
			t.Errorf("[%d] expected error, got nil", caseNum)
		}

		if !reflect.DeepEqual(resp, item.ResponseResult) {
			t.Errorf("[%d] wrong data: expected %#v, got %#v", caseNum, item.ResponseResult, resp)
		}

	}
	testServer.Close()

}

func TestBadResponseJson(t *testing.T) {
	testServer := httptest.NewServer(http.HandlerFunc(BadJSONResponseSearchServer))
	searchClient := &SearchClient{
		URL:         testServer.URL,
		AccessToken: "1",
	}
	testCase := TestCase{
		RequestParams: SearchRequest{
			Limit:      1,
			Offset:     0,
			Query:      "on",
			OrderField: "Name", // невозможно распаковать json ошибки
			OrderBy:    1,
		},
		ResponseResult: nil,
		HasError:       true,
		ErrorStr:       "cant unpack result json: unexpected end of JSON input",
	}
	_, err := searchClient.FindUsers(testCase.RequestParams)
	if err == nil {
		t.Errorf("expected error, got nil")
	} else if err.Error() != testCase.ErrorStr {
		t.Errorf("wrong error: expected %s, got %s", testCase.ErrorStr, err.Error())

	}
	testServer.Close()

}

func TestBadErrorJSON(t *testing.T) {
	testServer := httptest.NewServer(http.HandlerFunc(BadJSONSearchServer))
	searchClient := &SearchClient{
		URL:         testServer.URL,
		AccessToken: "1",
	}
	testCase := TestCase{
		RequestParams: SearchRequest{
			Limit:      1,
			Offset:     1,
			Query:      "on",
			OrderField: "badJson", // невозможно распаковать json ошибки
			OrderBy:    1,
		},
		ResponseResult: nil,
		HasError:       true,
		ErrorStr:       "cant unpack error json: unexpected end of JSON input",
	}
	_, err := searchClient.FindUsers(testCase.RequestParams)
	if err == nil {
		t.Errorf("expected error, got nil")
	} else if err.Error() != testCase.ErrorStr {
		t.Errorf("wrong error: expected %s, got %s", testCase.ErrorStr, err.Error())

	}
	testServer.Close()

}

func TestInternalServerError(t *testing.T) {
	PathFile = "qqqqq"
	defer func() {
		PathFile = "dataset.xml"
	}()
	testServer := httptest.NewServer(http.HandlerFunc(SearchServer))
	searchClient := &SearchClient{
		URL:         testServer.URL,
		AccessToken: "1",
	}
	testCase := TestCase{
		RequestParams: SearchRequest{
			Limit:      1,
			Offset:     1,
			Query:      "on",
			OrderField: "Name",
			OrderBy:    1,
		},
		ResponseResult: nil,
		HasError:       true,
		ErrorStr:       "SearchServer fatal error",
	}
	_, err := searchClient.FindUsers(testCase.RequestParams)
	if err == nil {
		t.Errorf("expected error, got nil")
	} else if err.Error() != testCase.ErrorStr {
		t.Errorf("wrong error: expected %s, got %s", testCase.ErrorStr, err.Error())

	}
	testServer.Close()

}

func TestTimeOut(t *testing.T) {
	testServer := httptest.NewServer(http.HandlerFunc(SlowSearchServer))
	searchClient := &SearchClient{
		URL:         testServer.URL,
		AccessToken: "1",
	}
	testCase := TestCase{
		RequestParams: SearchRequest{
			Limit:      2,
			Offset:     1,
			Query:      "on",
			OrderField: "Name",
			OrderBy:    1,
		},
		ResponseResult: nil,
		HasError:       true,
		ErrorStr:       "timeout for limit=3&offset=1&order_by=1&order_field=Name&query=on",
	}
	_, err := searchClient.FindUsers(testCase.RequestParams)
	if err == nil {
		t.Errorf("expected error, got nil")
	} else if err.Error() != testCase.ErrorStr {
		t.Errorf("wrong error: expected %s, got %s", testCase.ErrorStr, err.Error())

	}
	testServer.Close()

}

func TestNoAccessToken(t *testing.T) { // нет аксес токена
	testServer := httptest.NewServer(http.HandlerFunc(SearchServer))
	searchClient := &SearchClient{
		URL: testServer.URL,
	}
	testCase := TestCase{
		RequestParams: SearchRequest{
			Limit:      1,
			Offset:     1,
			Query:      "on",
			OrderField: "Name",
			OrderBy:    1,
		},
		ResponseResult: nil,
		HasError:       true,
		ErrorStr:       "bad AccessToken",
	}
	_, err := searchClient.FindUsers(testCase.RequestParams)
	if err == nil {
		t.Errorf("expected error, got nil")
	} else if err.Error() != testCase.ErrorStr {
		t.Errorf("wrong error: expected %s, got %s", testCase.ErrorStr, err.Error())

	}
	testServer.Close()

}

func TestSearchServer(t *testing.T) {
	testServer := httptest.NewServer(http.HandlerFunc(SearchServer))
	searchClient := &SearchClient{
		URL:         testServer.URL,
		AccessToken: "1",
	}
	testcases := []TestCase{
		{
			RequestParams: SearchRequest{
				Limit:      1,
				Offset:     1,
				Query:      "on",
				OrderField: "orderFieldError",
				OrderBy:    1,
			}, // Неправильный параметр в orderField
			ResponseResult: nil,
			HasError:       true,
			ErrorStr:       "OrderFeld" + " orderFieldError " + "invalid",
		},
		{
			RequestParams: SearchRequest{
				Limit:      1,
				Offset:     1,
				Query:      "on",
				OrderField: "Name",
				OrderBy:    4,
			}, // Неправильный параметр в orderBy(не то значение но типа инт)
			ResponseResult: nil,
			HasError:       true,
			ErrorStr:       "unknown bad request error: OrderBy invalid",
		},
		{
			RequestParams: SearchRequest{
				Limit:      -1,
				Offset:     1,
				Query:      "on",
				OrderField: "Name",
				OrderBy:    1,
			}, // Неправильный параметр в Limit
			ResponseResult: nil,
			HasError:       true,
			ErrorStr:       "limit must be > 0",
		},
		{
			RequestParams: SearchRequest{
				Limit:      1,
				Offset:     -1,
				Query:      "on",
				OrderField: "Age",
				OrderBy:    1,
			}, // Неправильный параметр в offset
			ResponseResult: nil,
			HasError:       true,
			ErrorStr:       "offset must be > 0",
		},
		{
			RequestParams: SearchRequest{
				Limit:      4,
				Offset:     0,
				Query:      "Beulah",
				OrderField: "",
				OrderBy:    1,
			},
			ResponseResult: &SearchResponse{
				Users: []User{ // успешный запрос выдающий 1 юзера
					{
						ID:     5,
						Name:   "Beulah Stark",
						Age:    30,
						About:  "Enim cillum eu cillum velit labore. In sint esse nulla occaecat voluptate pariatur aliqua aliqua non officia nulla aliqua. Fugiat nostrud irure officia minim cupidatat laborum ad incididunt dolore. Fugiat nostrud eiusmod ex ea nulla commodo. Reprehenderit sint qui anim non ad id adipisicing qui officia Lorem.",
						Gender: "female",
					},
				},
				NextPage: false,
			},
			HasError: false,
			ErrorStr: "",
		},
		{
			RequestParams: SearchRequest{
				Limit:      0, // успешный, но ничего не выдающий запрос так как лимит 0
				Offset:     1,
				Query:      "Beulah",
				OrderField: "",
				OrderBy:    0,
			},
			ResponseResult: &SearchResponse{
				Users:    []User{},
				NextPage: true,
			},
			HasError: false,
			ErrorStr: "",
		},
		{
			RequestParams: SearchRequest{
				Limit:      3,
				Offset:     1,
				Query:      "proident",
				OrderField: "ID",
				OrderBy:    -1,
			},
			ResponseResult: &SearchResponse{
				Users: []User{ // успешный запрос выдающий 3 юзеров в порядке убывания айди
					{
						ID:     31,
						Name:   "Palmer Scott",
						Age:    37,
						About:  "Elit fugiat commodo laborum quis eu consequat. In velit magna sit fugiat non proident ipsum tempor eu. Consectetur exercitation labore eiusmod occaecat adipisicing irure consequat fugiat ullamco aliquip nostrud anim irure enim. Duis do amet cillum eiusmod eu sunt. Minim minim sunt sit sit enim velit sint tempor enim sint aliquip voluptate reprehenderit officia. Voluptate magna sit consequat adipisicing ut eu qui.",
						Gender: "male",
					},
					{
						ID:     30,
						Name:   "Dickson Silva",
						Age:    32,
						About:  "Ipsum aliqua proident ullamco laboris eu occaecat deserunt. Amet ut adipisicing sint veniam dolore aliquip est mollit ex officia esse eiusmod veniam. Dolore magna minim aliquip sit deserunt. Nostrud occaecat dolore aliqua aliquip voluptate aliquip ad adipisicing.",
						Gender: "male",
					},
					{
						ID:     27,
						Name:   "Rebekah Sutton",
						Age:    26,
						About:  "Aliqua exercitation ad nostrud et exercitation amet quis cupidatat esse nostrud proident. Ullamco voluptate ex minim consectetur ea cupidatat in mollit reprehenderit voluptate labore sint laboris. Minim cillum et incididunt pariatur amet do esse. Amet irure elit deserunt quis culpa ut deserunt minim proident cupidatat nisi consequat ipsum.",
						Gender: "female",
					},
				},
				NextPage: true,
			},
			HasError: false,
			ErrorStr: "",
		},
		{
			RequestParams: SearchRequest{
				Limit:      2,
				Offset:     3,
				Query:      "",
				OrderField: "Age",
				OrderBy:    1,
			},
			ResponseResult: &SearchResponse{
				Users: []User{ // возвращает юзеров без поиска по ключевому слову не более 2х сортируя по возрасту по возрастанию
					{
						ID:     0,
						Name:   "Boyd Wolf",
						Age:    22,
						About:  "Nulla cillum enim voluptate consequat laborum esse excepteur occaecat commodo nostrud excepteur ut cupidatat. Occaecat minim incididunt ut proident ad sint nostrud ad laborum sint pariatur. Ut nulla commodo dolore officia. Consequat anim eiusmod amet commodo eiusmod deserunt culpa. Ea sit dolore nostrud cillum proident nisi mollit est Lorem pariatur. Lorem aute officia deserunt dolor nisi aliqua consequat nulla nostrud ipsum irure id deserunt dolore. Minim reprehenderit nulla exercitation labore ipsum.",
						Gender: "male",
					},
					{
						ID:     14,
						Name:   "Nicholson Newman",
						Age:    23,
						About:  "Tempor minim reprehenderit dolore et ad. Irure id fugiat incididunt do amet veniam ex consequat. Quis ad ipsum excepteur eiusmod mollit nulla amet velit quis duis ut irure.",
						Gender: "male",
					},
				},
				NextPage: true,
			},
			HasError: false,
			ErrorStr: "",
		},

		{
			RequestParams: SearchRequest{
				Limit:      2,
				Offset:     30,
				Query:      "",
				OrderField: "Age",
				OrderBy:    -1,
			},
			ResponseResult: &SearchResponse{
				Users: []User{ // возвращает юзеров без поиска по ключевому слову не более 2х сортируя по возрасту по убыванию
					{
						ID:     14,
						Name:   "Nicholson Newman",
						Age:    23,
						About:  "Tempor minim reprehenderit dolore et ad. Irure id fugiat incididunt do amet veniam ex consequat. Quis ad ipsum excepteur eiusmod mollit nulla amet velit quis duis ut irure.",
						Gender: "male",
					},
					{
						ID:     0,
						Name:   "Boyd Wolf",
						Age:    22,
						About:  "Nulla cillum enim voluptate consequat laborum esse excepteur occaecat commodo nostrud excepteur ut cupidatat. Occaecat minim incididunt ut proident ad sint nostrud ad laborum sint pariatur. Ut nulla commodo dolore officia. Consequat anim eiusmod amet commodo eiusmod deserunt culpa. Ea sit dolore nostrud cillum proident nisi mollit est Lorem pariatur. Lorem aute officia deserunt dolor nisi aliqua consequat nulla nostrud ipsum irure id deserunt dolore. Minim reprehenderit nulla exercitation labore ipsum.",
						Gender: "male",
					},
				},
				NextPage: true,
			},
			HasError: false,
			ErrorStr: "",
		},
		{
			RequestParams: SearchRequest{
				Limit:      2,
				Offset:     0,
				Query:      "A",
				OrderField: "Name",
				OrderBy:    1,
			},
			ResponseResult: &SearchResponse{
				Users: []User{ // возвращает юзеров не более 2х сортируя по имени по возрастанию
					{
						ID:     15,
						Name:   "Allison Valdez",
						Age:    21,
						About:  "Labore excepteur voluptate velit occaecat est nisi minim. Laborum ea et irure nostrud enim sit incididunt reprehenderit id est nostrud eu. Ullamco sint nisi voluptate cillum nostrud aliquip et minim. Enim duis esse do aute qui officia ipsum ut occaecat deserunt. Pariatur pariatur nisi do ad dolore reprehenderit et et enim esse dolor qui. Excepteur ullamco adipisicing qui adipisicing tempor minim aliquip.",
						Gender: "male",
					},
					{
						ID:     16,
						Name:   "Annie Osborn",
						Age:    35,
						About:  "Consequat fugiat veniam commodo nisi nostrud culpa pariatur. Aliquip velit adipisicing dolor et nostrud. Eu nostrud officia velit eiusmod ullamco duis eiusmod ad non do quis.",
						Gender: "female",
					},
				},
				NextPage: true,
			},
			HasError: false,
			ErrorStr: "",
		},

		{
			RequestParams: SearchRequest{
				Limit:      2,
				Offset:     16,
				Query:      "A",
				OrderField: "Name",
				OrderBy:    -1,
			},
			ResponseResult: &SearchResponse{
				Users: []User{ // возвращает юзеров не более 2х сортируя по имени по убыванию
					{
						ID:     16,
						Name:   "Annie Osborn",
						Age:    35,
						About:  "Consequat fugiat veniam commodo nisi nostrud culpa pariatur. Aliquip velit adipisicing dolor et nostrud. Eu nostrud officia velit eiusmod ullamco duis eiusmod ad non do quis.",
						Gender: "female",
					},
					{
						ID:     15,
						Name:   "Allison Valdez",
						Age:    21,
						About:  "Labore excepteur voluptate velit occaecat est nisi minim. Laborum ea et irure nostrud enim sit incididunt reprehenderit id est nostrud eu. Ullamco sint nisi voluptate cillum nostrud aliquip et minim. Enim duis esse do aute qui officia ipsum ut occaecat deserunt. Pariatur pariatur nisi do ad dolore reprehenderit et et enim esse dolor qui. Excepteur ullamco adipisicing qui adipisicing tempor minim aliquip.",
						Gender: "male",
					},
				},
				NextPage: false,
			},
			HasError: false,
			ErrorStr: "",
		},

		{
			RequestParams: SearchRequest{
				Limit:      30,
				Offset:     0,
				Query:      "",
				OrderField: "ID",
				OrderBy:    1,
			},
			ResponseResult: &SearchResponse{
				Users: []User{ // возвращает максимально возможное количество юзеров сортируя по возрастанию по айди
					{
						ID:     0,
						Name:   "Boyd Wolf",
						Age:    22,
						About:  "Nulla cillum enim voluptate consequat laborum esse excepteur occaecat commodo nostrud excepteur ut cupidatat. Occaecat minim incididunt ut proident ad sint nostrud ad laborum sint pariatur. Ut nulla commodo dolore officia. Consequat anim eiusmod amet commodo eiusmod deserunt culpa. Ea sit dolore nostrud cillum proident nisi mollit est Lorem pariatur. Lorem aute officia deserunt dolor nisi aliqua consequat nulla nostrud ipsum irure id deserunt dolore. Minim reprehenderit nulla exercitation labore ipsum.",
						Gender: "male",
					},
					{
						ID:     1,
						Name:   "Hilda Mayer",
						Age:    21,
						About:  "Sit commodo consectetur minim amet ex. Elit aute mollit fugiat labore sint ipsum dolor cupidatat qui reprehenderit. Eu nisi in exercitation culpa sint aliqua nulla nulla proident eu. Nisi reprehenderit anim cupidatat dolor incididunt laboris mollit magna commodo ex. Cupidatat sit id aliqua amet nisi et voluptate voluptate commodo ex eiusmod et nulla velit.",
						Gender: "female",
					},

					{
						ID:     2,
						Name:   "Brooks Aguilar",
						Age:    25,
						About:  "Velit ullamco est aliqua voluptate nisi do. Voluptate magna anim qui cillum aliqua sint veniam reprehenderit consectetur enim. Laborum dolore ut eiusmod ipsum ad anim est do tempor culpa ad do tempor. Nulla id aliqua dolore dolore adipisicing.",
						Gender: "male"},

					{
						ID:     3,
						Name:   "Everett Dillard",
						Age:    27,
						About:  "Sint eu id sint irure officia amet cillum. Amet consectetur enim mollit culpa laborum ipsum adipisicing est laboris. Adipisicing fugiat esse dolore aliquip quis laborum aliquip dolore. Pariatur do elit eu nostrud occaecat.",
						Gender: "male"},

					{
						ID:     4,
						Name:   "Owen Lynn",
						Age:    30,
						About:  "Elit anim elit eu et deserunt veniam laborum commodo irure nisi ut labore reprehenderit fugiat. Ipsum adipisicing labore ullamco occaecat ut. Ea deserunt ad dolor eiusmod aute non enim adipisicing sit ullamco est ullamco. Elit in proident pariatur elit ullamco quis. Exercitation amet nisi fugiat voluptate esse sit et consequat sit pariatur labore et.",
						Gender: "male"},

					{
						ID:     5,
						Name:   "Beulah Stark",
						Age:    30,
						About:  "Enim cillum eu cillum velit labore. In sint esse nulla occaecat voluptate pariatur aliqua aliqua non officia nulla aliqua. Fugiat nostrud irure officia minim cupidatat laborum ad incididunt dolore. Fugiat nostrud eiusmod ex ea nulla commodo. Reprehenderit sint qui anim non ad id adipisicing qui officia Lorem.",
						Gender: "female"},

					{
						ID:     6,
						Name:   "Jennings Mays",
						Age:    39,
						About:  "Veniam consectetur non non aliquip exercitation quis qui. Aliquip duis ut ad commodo consequat ipsum cupidatat id anim voluptate deserunt enim laboris. Sunt nostrud voluptate do est tempor esse anim pariatur. Ea do amet Lorem in mollit ipsum irure Lorem exercitation. Exercitation deserunt adipisicing nulla aute ex amet sint tempor incididunt magna. Quis et consectetur dolor nulla reprehenderit culpa laboris voluptate ut mollit. Qui ipsum nisi ullamco sit exercitation nisi magna fugiat anim consectetur officia.",
						Gender: "male"},

					{
						ID:     7,
						Name:   "Leann Travis",
						Age:    34,
						About:  "Lorem magna dolore et velit ut officia. Cupidatat deserunt elit mollit amet nulla voluptate sit. Quis aute aliquip officia deserunt sint sint nisi. Laboris sit et ea dolore consequat laboris non. Consequat do enim excepteur qui mollit consectetur eiusmod laborum ut duis mollit dolor est. Excepteur amet duis enim laborum aliqua nulla ea minim.",
						Gender: "female"},
					{
						ID:     8,
						Name:   "Glenn Jordan",
						Age:    29,
						About:  "Duis reprehenderit sit velit exercitation non aliqua magna quis ad excepteur anim. Eu cillum cupidatat sit magna cillum irure occaecat sunt officia officia deserunt irure. Cupidatat dolor cupidatat ipsum minim consequat Lorem adipisicing. Labore fugiat cupidatat nostrud voluptate ea eu pariatur non. Ipsum quis occaecat irure amet esse eu fugiat deserunt incididunt Lorem esse duis occaecat mollit.",
						Gender: "male"},

					{
						ID:     9,
						Name:   "Rose Carney",
						Age:    36,
						About:  "Voluptate ipsum ad consequat elit ipsum tempor irure consectetur amet. Et veniam sunt in sunt ipsum non elit ullamco est est eu. Exercitation ipsum do deserunt do eu adipisicing id deserunt duis nulla ullamco eu. Ad duis voluptate amet quis commodo nostrud occaecat minim occaecat commodo. Irure sint incididunt est cupidatat laborum in duis enim nulla duis ut in ut. Cupidatat ex incididunt do ullamco do laboris eiusmod quis nostrud excepteur quis ea.",
						Gender: "female"},

					{
						ID:    10,
						Name:  "Henderson Maxwell",
						Age:   30,
						About: "Ex et excepteur anim in eiusmod. Cupidatat sunt aliquip exercitation velit minim aliqua ad ipsum cillum dolor do sit dolore cillum. Exercitation eu in ex qui voluptate fugiat amet.", Gender: "male"},

					{
						ID:     11,
						Name:   "Gilmore Guerra",
						Age:    32,
						About:  "Labore consectetur do sit et mollit non incididunt. Amet aute voluptate enim et sit Lorem elit. Fugiat proident ullamco ullamco sint pariatur deserunt eu nulla consectetur culpa eiusmod. Veniam irure et deserunt consectetur incididunt ad ipsum sint. Consectetur voluptate adipisicing aute fugiat aliquip culpa qui nisi ut ex esse ex. Sint et anim aliqua pariatur.",
						Gender: "male"},

					{
						ID:     12,
						Name:   "Cruz Guerrero",
						Age:    36,
						About:  "Sunt enim ad fugiat minim id esse proident laborum magna magna. Velit anim aliqua nulla laborum consequat veniam reprehenderit enim fugiat ipsum mollit nisi. Nisi do reprehenderit aute sint sit culpa id Lorem proident id tempor. Irure ut ipsum sit non quis aliqua in voluptate magna. Ipsum non aliquip quis incididunt incididunt aute sint. Minim dolor in mollit aute duis consectetur.",
						Gender: "male"},

					{
						ID:     13,
						Name:   "Whitley Davidson",
						Age:    40,
						About:  "Consectetur dolore anim veniam aliqua deserunt officia eu. Et ullamco commodo ad officia duis ex incididunt proident consequat nostrud proident quis tempor. Sunt magna ad excepteur eu sint aliqua eiusmod deserunt proident. Do labore est dolore voluptate ullamco est dolore excepteur magna duis quis. Quis laborum deserunt ipsum velit occaecat est laborum enim aute. Officia dolore sit voluptate quis mollit veniam. Laborum nisi ullamco nisi sit nulla cillum et id nisi.",
						Gender: "male"},

					{
						ID:     14,
						Name:   "Nicholson Newman",
						Age:    23,
						About:  "Tempor minim reprehenderit dolore et ad. Irure id fugiat incididunt do amet veniam ex consequat. Quis ad ipsum excepteur eiusmod mollit nulla amet velit quis duis ut irure.",
						Gender: "male"},

					{
						ID:     15,
						Name:   "Allison Valdez",
						Age:    21,
						About:  "Labore excepteur voluptate velit occaecat est nisi minim. Laborum ea et irure nostrud enim sit incididunt reprehenderit id est nostrud eu. Ullamco sint nisi voluptate cillum nostrud aliquip et minim. Enim duis esse do aute qui officia ipsum ut occaecat deserunt. Pariatur pariatur nisi do ad dolore reprehenderit et et enim esse dolor qui. Excepteur ullamco adipisicing qui adipisicing tempor minim aliquip.",
						Gender: "male"},

					{
						ID:     16,
						Name:   "Annie Osborn",
						Age:    35,
						About:  "Consequat fugiat veniam commodo nisi nostrud culpa pariatur. Aliquip velit adipisicing dolor et nostrud. Eu nostrud officia velit eiusmod ullamco duis eiusmod ad non do quis.",
						Gender: "female"},

					{
						ID:     17,
						Name:   "Dillard Mccoy",
						Age:    36,
						About:  "Laborum voluptate sit ipsum tempor dolore. Adipisicing reprehenderit minim aliqua est. Consectetur enim deserunt incididunt elit non consectetur nisi esse ut dolore officia do ipsum.",
						Gender: "male"},

					{
						ID:     18,
						Name:   "Terrell Hall",
						Age:    27,
						About:  "Ut nostrud est est elit incididunt consequat sunt ut aliqua sunt sunt. Quis consectetur amet occaecat nostrud duis. Fugiat in irure consequat laborum ipsum tempor non deserunt laboris id ullamco cupidatat sit. Officia cupidatat aliqua veniam et ipsum labore eu do aliquip elit cillum. Labore culpa exercitation sint sint.",
						Gender: "male"},

					{
						ID:     19,
						Name:   "Bell Bauer",
						Age:    26,
						About:  "Nulla voluptate nostrud nostrud do ut tempor et quis non aliqua cillum in duis. Sit ipsum sit ut non proident exercitation. Quis consequat laboris deserunt adipisicing eiusmod non cillum magna.",
						Gender: "male"},

					{
						ID:     20,
						Name:   "Lowery York",
						Age:    27,
						About:  "Dolor enim sit id dolore enim sint nostrud deserunt. Occaecat minim enim veniam proident mollit Lorem irure ex. Adipisicing pariatur adipisicing aliqua amet proident velit. Magna commodo culpa sit id.",
						Gender: "male"},

					{
						ID:     21,
						Name:   "Johns Whitney",
						Age:    26,
						About:  "Elit sunt exercitation incididunt est ea quis do ad magna. Commodo laboris nisi aliqua eu incididunt eu irure. Labore ullamco quis deserunt non cupidatat sint aute in incididunt deserunt elit velit. Duis est mollit veniam aliquip. Nulla sunt veniam anim et sint dolore.",
						Gender: "male"},

					{ID: 22,
						Name:   "Beth Wynn",
						Age:    31,
						About:  "Proident non nisi dolore id non. Aliquip ex anim cupidatat dolore amet veniam tempor non adipisicing. Aliqua adipisicing eu esse quis reprehenderit est irure cillum duis dolor ex. Laborum do aute commodo amet. Fugiat aute in excepteur ut aliqua sint fugiat do nostrud voluptate duis do deserunt. Elit esse ipsum duis ipsum.",
						Gender: "female"},

					{
						ID:     23,
						Name:   "Gates Spencer",
						Age:    21,
						About:  "Dolore magna magna commodo irure. Proident culpa nisi veniam excepteur sunt qui et laborum tempor. Qui proident Lorem commodo dolore ipsum.",
						Gender: "male"},

					{
						ID:     24,
						Name:   "Gonzalez Anderson",
						Age:    33,
						About:  "Quis consequat incididunt in ex deserunt minim aliqua ea duis. Culpa nisi excepteur sint est fugiat cupidatat nulla magna do id dolore laboris. Aute cillum eiusmod do amet dolore labore commodo do pariatur sit id. Do irure eiusmod reprehenderit non in duis sunt ex. Labore commodo labore pariatur ex minim qui sit elit.",
						Gender: "male"},
				},
				NextPage: true,
			},
			HasError: false,
			ErrorStr: "",
		},
	}

	for caseNum, item := range testcases {
		resp, err := searchClient.FindUsers(item.RequestParams)
		if err != nil {
			if !item.HasError {
				t.Errorf("[%d] unexpected error %s", caseNum, err.Error())
			} else if err.Error() != item.ErrorStr {
				t.Errorf("[%d] wrong error: expected %s, got %s", caseNum, item.ErrorStr, err.Error())

			}

		}
		if err == nil && item.HasError {
			t.Errorf("[%d] expected error, got nil", caseNum)
		}

		if !reflect.DeepEqual(resp, item.ResponseResult) {
			t.Errorf("[%d] wrong data: expected %#v, got %#v", caseNum, item.ResponseResult, resp)
		}

	}
	testServer.Close()

}
