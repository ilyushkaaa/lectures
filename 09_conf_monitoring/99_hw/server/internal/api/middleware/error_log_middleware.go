package middleware

import (
	"errors"
	"github.com/labstack/echo"
)

var ErrNoLogger = errors.New("no logger in context")

func ErrorLogMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(context echo.Context) error {
			myLogger, err := GetLoggerFromContext(context)
			if err != nil {
				return err
			}
			myLogger.Infof("error log middleware call")
			err = next(context)
			if err != nil {
				myLogger.Errorw("Request error:",
					"error", err,
					"method", context.Request().Method,
					"remote_addr", context.RealIP(),
					"url", context.Request().URL.Path)
			}
			return err
		}
	}
}
