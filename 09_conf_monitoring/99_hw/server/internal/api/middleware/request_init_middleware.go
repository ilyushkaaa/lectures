package middleware

import (
	"github.com/google/uuid"
	"github.com/labstack/echo"
	"go.uber.org/zap"
	"log"
)

func RequestInitMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(context echo.Context) error {
			myLogger, err := initLogger()
			if err != nil {
				return err
			}
			myLogger = myLogger.With(zap.String("request-id", uuid.New().String()))
			context.Set("myLogger", myLogger)
			myLogger.Infof("request init middleware call")

			err = next(context)
			loggerErr := myLogger.Sync() // Вызываем Sync() в конце обработки
			if loggerErr != nil {
				log.Println("error in logger sync")
			}
			return err
		}
	}
}

func initLogger() (*zap.SugaredLogger, error) {
	zapLogger, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("error in logger initialization: %s", err)
		return nil, err
	}
	myLogger := zapLogger.Sugar()
	return myLogger, nil
}
