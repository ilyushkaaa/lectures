package middleware

import (
	"github.com/labstack/echo"
	"go.uber.org/zap"
	"time"
)

func AccessLogMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(context echo.Context) error {
			myLogger, err := GetLoggerFromContext(context)
			if err != nil {
				return err
			}
			myLogger.Infof("access log middleware call")
			startTime := time.Now()
			err = next(context)
			finishTime := time.Now()

			myLogger.Infow("Request result",
				"method", context.Request().Method,
				"remote_addr", context.RealIP(),
				"url", context.Request().URL.Path,
				"time of call", startTime.Format(time.RFC3339),
				"time", finishTime.Sub(startTime).Seconds(),
				"response status", context.Response().Status,
			)
			return err

		}
	}
}

func GetLoggerFromContext(ctx echo.Context) (*zap.SugaredLogger, error) {
	myLogger, ok := ctx.Get("myLogger").(*zap.SugaredLogger)
	if !ok {
		return myLogger, ErrNoLogger
	}
	return myLogger, nil
}
