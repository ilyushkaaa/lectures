package middleware

import (
	"github.com/labstack/echo"
	"github.com/prometheus/client_golang/prometheus"
	"strconv"
	"time"
)

func MetricMiddleware(hits *prometheus.CounterVec, timings *prometheus.HistogramVec) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(context echo.Context) error {
			myLogger, err := GetLoggerFromContext(context)
			if err != nil {
				return err
			}
			myLogger.Infof("metric middleware call")
			startTime := time.Now()
			err = next(context)
			endTime := time.Now()
			hits.WithLabelValues(strconv.Itoa(context.Response().Status), context.Request().URL.String()).Inc()
			timings.WithLabelValues(strconv.Itoa(context.Response().Status),
				context.Request().URL.String()).Observe(endTime.Sub(startTime).Seconds())
			return err

		}
	}
}
