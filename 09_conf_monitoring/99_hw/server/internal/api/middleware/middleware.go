package middleware

import (
	"server/internal/pkg/domain"

	"github.com/labstack/echo"
)

func AuthEchoMiddleware(service domain.SessionService) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(context echo.Context) error {
			myLogger, err := GetLoggerFromContext(context)
			if err != nil {
				return err
			}
			myLogger.Infof("Auth Echo Middleware call")
			_, err = service.CheckSession(context.Request().Header, myLogger)
			if err != nil {
				myLogger.Error("error: no session ")
				return context.NoContent(401)
			}
			return next(context)
		}
	}
}
