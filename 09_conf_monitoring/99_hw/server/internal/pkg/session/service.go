package session

import (
	"errors"
	"github.com/prometheus/client_golang/prometheus"
	"go.uber.org/zap"
	"net/http"
	"server/internal/pkg/domain"
	"strconv"
	"time"
)

type service struct {
	hits    *prometheus.CounterVec
	timings *prometheus.HistogramVec
}

func NewService(hits *prometheus.CounterVec, timings *prometheus.HistogramVec) domain.SessionService {
	return service{
		hits:    hits,
		timings: timings,
	}
}

func (s service) CheckSession(headers http.Header, logger *zap.SugaredLogger) (domain.Session, error) {
	req, err := http.NewRequest(http.MethodGet, "http://vk-golang.ru:17000/int/CheckSession?stable=true&light=true", nil)
	if err != nil {
		logger.Errorf("error: can not make request for check session")
		return domain.Session{}, err
	}

	req.Header = headers

	startTime := time.Now()
	resp, err := http.DefaultClient.Do(req)
	finishTime := time.Now()
	if err != nil {
		logger.Errorf("error: can not get response for request %v", req)
		return domain.Session{}, err
	}

	s.LogRequestToRemoteServer(logger, startTime, finishTime, req, resp)
	s.MakeMetricOfRemoteServer(startTime, finishTime, req, resp)

	switch resp.StatusCode {
	case 500:
		logger.Errorf("request %b: CHECK SESSION: internal server error: can not get session from remote server: status")
		return domain.Session{}, errors.New("failed to request check session")
	case 200:
		return domain.Session{}, nil
	default:
		logger.Infof("CHECK SESSION: user has not got session, headers: %s", headers)
		return domain.Session{}, domain.ErrNoSession
	}
}

func (s service) LogRequestToRemoteServer(logger *zap.SugaredLogger, timeStart time.Time, finishTime time.Time, req *http.Request, resp *http.Response) {
	logger.Infow("Thread remote server request:",
		"method", req.Method,
		"remote_addr", req.RemoteAddr,
		"url", req.URL.Path,
		"start time of request", timeStart.Format(time.RFC3339),
		"time", finishTime.Sub(timeStart).Seconds(),
		"status code", resp.StatusCode)
}

func (s service) MakeMetricOfRemoteServer(startTime time.Time, finishTime time.Time, req *http.Request, resp *http.Response) {
	s.hits.WithLabelValues(strconv.Itoa(resp.StatusCode), req.URL.String()).Inc()
	s.timings.WithLabelValues(strconv.Itoa(resp.StatusCode), req.URL.String()).Observe(finishTime.Sub(startTime).Seconds())
}
