package service

import (
	"go.uber.org/zap"
	"server/internal/pkg/domain"
)

type service struct {
	ThreadRepo domain.ThreadRepository
}

func NewService(threadRepo domain.ThreadRepository) domain.ThreadService {
	return service{
		ThreadRepo: threadRepo,
	}
}

func (s service) Create(thread domain.Thread, logger *zap.SugaredLogger) error {
	return s.ThreadRepo.Create(thread, logger)
}

func (s service) Get(id string, logger *zap.SugaredLogger) (domain.Thread, error) {
	return s.ThreadRepo.Get(id, logger)
}
