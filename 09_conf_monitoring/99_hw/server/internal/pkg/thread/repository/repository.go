package repository

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"go.uber.org/zap"
	"net/http"
	"server/internal/pkg/domain"
	"strconv"
	"time"
)

type repository struct {
	logger  *zap.SugaredLogger
	hits    *prometheus.CounterVec
	timings *prometheus.HistogramVec
}

func (r repository) Create(thread domain.Thread, logger *zap.SugaredLogger) error {
	reqBody, err := json.Marshal(thread)
	if err != nil {
		logger.Errorf("json marshal error: %s for thread %v", err, thread)
		return err
	}

	req, err := http.NewRequest(http.MethodPost, "http://vk-golang.ru:15000/thread?stable=true", bytes.NewBuffer(reqBody))
	if err != nil {
		logger.Errorf("error: can not make request for thread create with request body %s", reqBody)
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	startTime := time.Now()
	resp, err := http.DefaultClient.Do(req)
	finishTime := time.Now()
	if err != nil {
		r.logger.Errorf("error: can not get response for request %v", req)
		return err
	}

	r.LogRequestToRemoteServer(logger, startTime, finishTime, req, resp)
	r.MakeMetricOfRemoteServer(startTime, finishTime, req, resp)
	if resp.StatusCode != 200 {
		r.logger.Errorf("CREATE THREAD: remote server response has not status 200 for request %v", req)
		return errors.New("failed to create thread remotely")
	}

	return nil
}

func (r repository) Get(id string, logger *zap.SugaredLogger) (domain.Thread, error) {
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("http://vk-golang.ru:15000/thread?id=%s&thread_fast=true", id), nil)
	if err != nil {
		r.logger.Errorf("error: can not make request for get thred with request body")
		return domain.Thread{}, err
	}

	startTime := time.Now()
	resp, err := http.DefaultClient.Do(req)
	finishTime := time.Now()
	if err != nil {
		r.logger.Errorf("error: can not get response for request %v", req)
		return domain.Thread{}, err
	}

	r.LogRequestToRemoteServer(logger, startTime, finishTime, req, resp)
	r.MakeMetricOfRemoteServer(startTime, finishTime, req, resp)

	if resp.StatusCode != 200 {
		r.logger.Errorf("GET THREAD: remote server response has not status 200 for request %v", req)
		return domain.Thread{}, errors.New("failed to fetch thread remotely")
	}

	var thread domain.Thread
	err = json.NewDecoder(resp.Body).Decode(&thread)
	if err != nil {
		r.logger.Errorf("GET THREAD: can not decode remote server reponse %s", resp.Body)
		return domain.Thread{}, err
	}

	return thread, nil
}

func NewRepository(hits *prometheus.CounterVec, timings *prometheus.HistogramVec) domain.ThreadRepository {
	return repository{
		hits:    hits,
		timings: timings,
	}
}

func (r repository) LogRequestToRemoteServer(logger *zap.SugaredLogger, timeStart time.Time, finishTime time.Time, req *http.Request, resp *http.Response) {
	logger.Infow("Thread remote server request:",
		"method", req.Method,
		"remote_addr", req.RemoteAddr,
		"url", req.URL.Path,
		"start time of request", timeStart.Format(time.RFC3339),
		"time", finishTime.Sub(timeStart).Seconds(),
		"status code", resp.StatusCode)
}

func (r repository) MakeMetricOfRemoteServer(startTime time.Time, finishTime time.Time, req *http.Request, resp *http.Response) {
	r.hits.WithLabelValues(strconv.Itoa(resp.StatusCode), req.URL.String()).Inc()
	r.timings.WithLabelValues(strconv.Itoa(resp.StatusCode), req.URL.String()).Observe(finishTime.Sub(startTime).Seconds())
}
