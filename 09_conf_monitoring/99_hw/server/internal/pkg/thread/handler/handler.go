package handler

import (
	"server/internal/api/middleware"
	"server/internal/pkg/domain"

	"github.com/labstack/echo"
)

type Handler struct {
	ThreadSvc domain.ThreadService
}

func (h Handler) GetThread(ctx echo.Context) error {
	tid := ctx.Param("tid")

	myLogger, err := middleware.GetLoggerFromContext(ctx)
	if err != nil {
		return err
	}
	myLogger.Infof("repository get thread call")
	t, err := h.ThreadSvc.Get(tid, myLogger)
	if err != nil {
		return err
	}

	return ctx.JSON(200, t)
}

func (h Handler) CreateThread(ctx echo.Context) error {
	var thread domain.Thread

	err := ctx.Bind(&thread)
	if err != nil {
		return err
	}

	myLogger, err := middleware.GetLoggerFromContext(ctx)
	if err != nil {
		return err
	}
	myLogger.Infof("repository create thread call")
	err = h.ThreadSvc.Create(thread, myLogger)
	if err != nil {
		return err
	}

	return ctx.NoContent(200)
}
