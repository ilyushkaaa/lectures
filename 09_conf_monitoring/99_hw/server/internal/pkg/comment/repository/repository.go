package repository

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"go.uber.org/zap"
	"net/http"
	"server/internal/pkg/domain"
	"strconv"
	"time"
)

type repository struct {
	hits    *prometheus.CounterVec
	timings *prometheus.HistogramVec
}

func NewRepository(hits *prometheus.CounterVec, timings *prometheus.HistogramVec) domain.CommentRepository {
	return repository{
		hits:    hits,
		timings: timings,
	}
}

func (r repository) Create(comment domain.Comment, logger *zap.SugaredLogger) error {
	reqBody, err := json.Marshal(comment)
	if err != nil {
		logger.Errorf("json marshal error: %s for comment %v", err, comment)
		return err
	}

	req, err := http.NewRequest(http.MethodPost, "http://vk-golang.ru:16000/comment?fast=true", bytes.NewBuffer(reqBody))
	if err != nil {
		logger.Errorf("error: can not make request for comment create with request body %s", reqBody)
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	startTime := time.Now()
	resp, err := http.DefaultClient.Do(req)
	finishTime := time.Now()
	if err != nil {
		logger.Errorf("error: can not get response for request %v", req)
		return err
	}

	r.LogRequestToRemoteServer(logger, startTime, finishTime, req, resp)
	r.MakeMetricOfRemoteServer(startTime, finishTime, req, resp)

	if resp.StatusCode != 200 {
		logger.Errorf("CREATE COMMENT: remote server response status code is not 200 for request %v", req)
		return errors.New("failed to create comment remotely")
	}

	return nil
}

func (r repository) Like(commentID string, logger *zap.SugaredLogger) error {
	req, err := http.NewRequest(
		http.MethodPost,
		fmt.Sprintf("http://vk-golang.ru:16000/comment/like?cid=%s&superstable=true", commentID),
		nil,
	)
	if err != nil {
		logger.Errorf("error: can not make request for comment like")
		return err
	}

	startTime := time.Now()
	resp, err := http.DefaultClient.Do(req)
	finishTime := time.Now()
	if err != nil {
		logger.Errorf("error: can not get response for request %v", req)
		return err
	}

	r.LogRequestToRemoteServer(logger, startTime, finishTime, req, resp)
	r.MakeMetricOfRemoteServer(startTime, finishTime, req, resp)

	if resp.StatusCode != 200 {
		logger.Errorf("LIKE COMMENT: remote server response status code is not 200 for request %v", req)
		return errors.New("failed to like comment remotely")
	}

	return nil
}

func (r repository) LogRequestToRemoteServer(logger *zap.SugaredLogger, timeStart time.Time, finishTime time.Time, req *http.Request, resp *http.Response) {
	logger.Infow("Thread remote server request:",
		"method", req.Method,
		"remote_addr", req.RemoteAddr,
		"url", req.URL.Path,
		"start time of request", timeStart.Format(time.RFC3339),
		"time", finishTime.Sub(timeStart).Seconds(),
		"status code", resp.StatusCode)
}
func (r repository) MakeMetricOfRemoteServer(startTime time.Time, finishTime time.Time, req *http.Request, resp *http.Response) {
	r.hits.WithLabelValues(strconv.Itoa(resp.StatusCode), req.URL.String()).Inc()
	r.timings.WithLabelValues(strconv.Itoa(resp.StatusCode), req.URL.String()).Observe(finishTime.Sub(startTime).Seconds())
}
