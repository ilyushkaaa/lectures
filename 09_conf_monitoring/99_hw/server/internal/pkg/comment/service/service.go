package service

import (
	"go.uber.org/zap"
	"server/internal/pkg/domain"
)

type service struct {
	CommentRepo domain.CommentRepository
	ThreadRepo  domain.ThreadRepository
}

func NewService(commentRepo domain.CommentRepository, threadRepo domain.ThreadRepository) domain.CommentService {
	return service{
		CommentRepo: commentRepo,
		ThreadRepo:  threadRepo,
	}
}

func (s service) Create(threadID string, comment domain.Comment, logger *zap.SugaredLogger) error {
	if err := s.checkThread(threadID, logger); err != nil {
		return err
	}

	return s.CommentRepo.Create(comment, logger)
}

func (s service) Like(threadID string, commentID string, logger *zap.SugaredLogger) error {
	if err := s.checkThread(threadID, logger); err != nil {
		return err
	}

	return s.CommentRepo.Like(commentID, logger)
}

func (s service) checkThread(threadID string, logger *zap.SugaredLogger) error {
	_, err := s.ThreadRepo.Get(threadID, logger)
	return err
}
