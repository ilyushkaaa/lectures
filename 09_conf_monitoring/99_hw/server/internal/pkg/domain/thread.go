package domain

import (
	"go.uber.org/zap"
)

type Thread struct {
	ID   string
	Name string
}

type ThreadService interface {
	Create(thread Thread, logger *zap.SugaredLogger) error
	Get(id string, logger *zap.SugaredLogger) (Thread, error)
}

type ThreadRepository interface {
	Create(thread Thread, logger *zap.SugaredLogger) error
	Get(id string, logger *zap.SugaredLogger) (Thread, error)
}
