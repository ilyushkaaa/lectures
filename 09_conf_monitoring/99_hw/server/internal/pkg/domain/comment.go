package domain

import (
	"go.uber.org/zap"
)

type Comment struct {
	ID   string
	Text string
}

type CommentService interface {
	Create(threadID string, comment Comment, logger *zap.SugaredLogger) error
	Like(threadID string, commentID string, logger *zap.SugaredLogger) error
}

type CommentRepository interface {
	Create(comment Comment, logger *zap.SugaredLogger) error
	Like(commentID string, logger *zap.SugaredLogger) error
}
