package domain

import (
	"errors"
	"go.uber.org/zap"
	"net/http"
)

var ErrNoSession = errors.New("no session")

type Session struct {
	UserID string
}

type SessionService interface {
	CheckSession(headers http.Header, logger *zap.SugaredLogger) (Session, error)
}
