module server

go 1.15

require (
	github.com/google/uuid v1.5.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/prometheus/client_golang v1.17.0
	go.uber.org/zap v1.26.0
)
