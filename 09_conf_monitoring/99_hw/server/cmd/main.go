package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"log"
	"server/internal/api/middleware"
	"server/internal/pkg/comment/handler"
	commentrepo "server/internal/pkg/comment/repository"
	commentsvc "server/internal/pkg/comment/service"
	"server/internal/pkg/session"
	threadhttp "server/internal/pkg/thread/handler"
	threadrepo "server/internal/pkg/thread/repository"
	threadsvc "server/internal/pkg/thread/service"

	"github.com/labstack/echo"
)

func main() {
	mainRouter := echo.New()
	mainRouter.GET("/metrics", echo.WrapHandler(promhttp.Handler()))
	e := mainRouter.Group("")

	hitsSessionService := createPrometheusCounterVec("hits_session_service")
	hitsCommentService := createPrometheusCounterVec("hits_comment_service")
	hitsThreadService := createPrometheusCounterVec("hits_thread_service")
	hitsApp := createPrometheusCounterVec("hitsApp")

	timingsSessionService := createPrometheusHistogramVec("timings_session_service", 0, 0.005, 3)
	timingsCommentService := createPrometheusHistogramVec("timings_comment_service", 0, 0.02, 3)
	timingsThreadService := createPrometheusHistogramVec("timings_thread_service", 0, 0.01, 3)
	timingsApp := createPrometheusHistogramVec("timings_app", 0, 0.01, 5)

	prometheus.MustRegister(hitsSessionService, hitsCommentService, hitsThreadService, hitsApp, timingsApp,
		timingsCommentService, timingsSessionService, timingsThreadService)

	sessionSvc := session.NewService(hitsSessionService, timingsSessionService)
	e.Use(middleware.RequestInitMiddleware())
	e.Use(middleware.AccessLogMiddleware())
	e.Use(middleware.ErrorLogMiddleware())
	e.Use(middleware.MetricMiddleware(hitsApp, timingsApp))
	e.Use(middleware.AuthEchoMiddleware(sessionSvc))

	threadRepo := threadrepo.NewRepository(hitsThreadService, timingsThreadService)
	threadSvc := threadsvc.NewService(threadRepo)
	threadHandler := threadhttp.Handler{ThreadSvc: threadSvc}

	commentRepo := commentrepo.NewRepository(hitsCommentService, timingsCommentService)
	commentSvc := commentsvc.NewService(commentRepo, threadRepo)
	commentHandler := handler.Handler{CommentSvc: commentSvc}

	e.GET("/thread/:id", threadHandler.GetThread)
	e.POST("/thread", threadHandler.CreateThread)
	e.POST("/thread/:tid/comment", commentHandler.Create)
	e.POST("/thread/:tid/comment/:cid/like", commentHandler.Like)

	err := mainRouter.Start(":8000")
	if err != nil {
		log.Fatal("can not start server")
	}
}

func createPrometheusCounterVec(name string) *prometheus.CounterVec {
	return prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: name,
	}, []string{"status", "path"})
}

func createPrometheusHistogramVec(name string, minVal, interval float64, bucketsNum int) *prometheus.HistogramVec {
	return prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name:    name,
		Help:    "Duration of HTTP requests",
		Buckets: prometheus.LinearBuckets(minVal, interval, bucketsNum),
	}, []string{"status", "path"})
}
