package main

import (
	"errors"
	"reflect"
)

var errorBadFieldType = errors.New("bad field type")

func i2s(data interface{}, out interface{}) error {
	valOut := reflect.ValueOf(out)
	valData := reflect.ValueOf(data)
	if valOut.Kind() != reflect.Ptr {
		return errorBadFieldType
	}
	valOut = valOut.Elem()
	return switchUnpack(valData, valOut)
}

func unpackStruct(valData reflect.Value, valOut reflect.Value) error {
	outType := valOut.Type()
	for i := 0; i < valOut.NumField(); i++ {
		valOutField := valOut.Field(i)
		valDataFromMap := valData.MapIndex(reflect.ValueOf(outType.Field(i).Name))
		if valDataFromMap.IsValid() {
			valDataField := valDataFromMap.Elem()
			err := switchUnpack(valDataField, valOutField)
			if err != nil {
				return err
			}
		} else {
			return errorBadFieldType
		}
	}
	return nil
}

func unpackSlice(valData reflect.Value, valOut reflect.Value) error {
	newOut := reflect.New(valOut.Type().Elem()).Elem()
	for i := 0; i < valData.Len(); i++ {
		currentValData := valData.Index(i)
		currentValDataValue := reflect.ValueOf(currentValData.Interface())
		err := switchUnpack(currentValDataValue, newOut)
		if err != nil {
			return err
		}
		valOut.Set(reflect.Append(valOut, newOut))
	}
	return nil
}

func switchUnpack(valData, valOut reflect.Value) error {
	switch valOut.Kind() {
	case reflect.Int:
		if valData.Type().Kind() == reflect.Float64 {
			valOut.Set(reflect.ValueOf(int(valData.Float())))
		} else {
			return errorBadFieldType
		}
	case reflect.Struct:
		if valData.Type().Kind() == reflect.Map {
			err := unpackStruct(valData, valOut)
			if err != nil {
				return err
			}
		} else {
			return errorBadFieldType
		}
	case reflect.Slice:
		if valData.Type().Kind() == reflect.Slice {
			err := unpackSlice(valData, valOut)
			if err != nil {
				return err
			}
		} else {
			return errorBadFieldType
		}
	default:
		if valOut.Kind() == valData.Type().Kind() && valOut.CanSet() {
			valOut.Set(valData)
		} else {
			return errorBadFieldType
		}
	}
	return nil
}
