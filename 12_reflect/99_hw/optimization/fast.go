package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

type User struct {
	Browsers []string
	Email    string
	Name     string
}

func FastSearch(out io.Writer) {
	file, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	decoder := json.NewDecoder(file)
	seenBrowsers := make(map[string]struct{})
	uniqueBrowsers := 0
	userCount := 0
	isAndroid := false
	isMSIE := false
	var user User
	_, err = fmt.Fprintln(out, "found users:")
	if err != nil {
		log.Fatal("can not write response")
		return
	}
	for {
		if err = decoder.Decode(&user); err != nil {
			if err == io.EOF {
				break
			}
			fmt.Println("Error decoding JSON:", err)
			return
		}
		userCount++
		if err != nil {
			return
		}
		isAndroid = false
		isMSIE = false
		for _, browser := range user.Browsers {
			if strings.Contains(browser, "Android") {
				isAndroid = true
				_, ok := seenBrowsers[browser]
				if !ok {
					seenBrowsers[browser] = struct{}{}
					uniqueBrowsers++
				}
			}

			if strings.Contains(browser, "MSIE") {
				isMSIE = true
				_, ok := seenBrowsers[browser]
				if !ok {
					seenBrowsers[browser] = struct{}{}
					uniqueBrowsers++
				}
			}
		}
		if isAndroid && isMSIE {
			if idx := strings.IndexByte(user.Email, '@'); idx != -1 {
				var buff bytes.Buffer
				buff.Grow(len(user.Email) + 15)
				buff.WriteString("[")
				buff.WriteString(fmt.Sprint(userCount - 1))
				buff.WriteString("] ")
				buff.WriteString(user.Name)
				buff.WriteString(" <")
				buff.WriteString(user.Email[:idx])
				buff.WriteString(" [at] ")
				buff.WriteString(user.Email[idx+1:])
				buff.WriteString(">")
				_, err = fmt.Fprintln(out, buff.String())
				if err != nil {
					log.Fatal("can not write response")

					return
				}
			}
		}
	}
	_, err = fmt.Fprintln(out, "\nTotal unique browsers", len(seenBrowsers))
	if err != nil {
		log.Fatal("can not write response")
		return
	}
}
